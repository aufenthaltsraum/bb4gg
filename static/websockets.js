export function setUpWebSockets (channel, handler) {
  let protocol = 'ws://';
  if (window.location.protocol === 'https:') {
    protocol = 'wss://';
  }
  const instanceName =
    document.querySelector('meta[name="instance-name"]').content;
  const url =
    protocol + window.location.host + '/' + instanceName + '/websockets/';
  const ws = new WebSocket(url);

  ws.onopen = function () {
    ws.send(channel);
  };

  ws.onmessage = handler;

  ws.onclose = function (event) {
    if(!event.wasClean) {
      document.getElementById('disconectMsg').style.display = '';
    }
  };
};
