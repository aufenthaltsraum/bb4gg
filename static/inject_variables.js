export const injectedVariables = {};

export function getGlobals() {
    for(const meta of document.querySelectorAll('meta')) {
        injectedVariables[meta.name] = meta.content;
    }
}

getGlobals();
