function greyOutForm(form) {
    let formWrap = form.closest('.form-wrap');
    if (!formWrap) {
        formWrap = form;
    }
    formWrap.classList.add('disabled');
}

// Will be added automatically to all forms with class submit-background
export function sendForm(event) {
    event.preventDefault(); // Do not send form as usual.

    const form = event.target.form || event.target;
    greyOutForm(form);
    const endpoint = form.action;
    const method = form.method; // Should always be POST, but we can be general

    // URLSearchParams makes a typical query-string (key=value&foo=bar) from the FormData
    const data = new URLSearchParams(new FormData(form));

    fetch(endpoint, {
        method: method,
        body: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
    });
    return false;
}

export function setSubmitListeners() {
    for(const form of document.querySelectorAll('.submit-background')) {
        form.addEventListener('submit', sendForm);
    }
}
