// We set event listeners for the body, so they do not need to be renewed when parts of the page are updated.

// With a mouse the tooltip should be shown on hover.
if (window.matchMedia('(any-hover: hover)').matches) {
  document.body.addEventListener("mouseover", (e) => {
    let hasTooltip = e.target.closest("details.with-tooltip > summary");
    if (hasTooltip) {
      hasTooltip.parentElement.open = true;
    }
  });

  document.body.addEventListener("mouseout", (e) => {
    let hasTooltip = e.target.closest("details.with-tooltip > summary:not(:hover)");
    if (hasTooltip) {
      hasTooltip.parentElement.open = false;
    }
  });

  // We don't want clicking to toggle the tooltip.
  document.body.addEventListener("click", (e) => {
    let withTooltip = e.target.closest("details.with-tooltip");
    if (withTooltip) { e.preventDefault(); }
  });
}
// With a phone or similar the tooltip should be shown on click (which is the standard behavior).
else {
  // The tooltip should be hidden again when somewhere else is clicked.
  document.body.addEventListener("focusout", (e) => {
    let withTooltip = e.target.closest("details.with-tooltip");
    if (withTooltip) { withTooltip.open = false; }
  });
}
