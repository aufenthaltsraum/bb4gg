<details class="with-tooltip">
  <summary>
    <apply-content />
  </summary>
  <div class="tooltip-box">
    <span class="tooltip">
      <tooltip />
    </span>
  </div>
</details>