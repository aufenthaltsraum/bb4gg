<apply template="/base">
  <bind tag="page-title">
    <page-name />
  </bind>
  <bind tag="page-headers">
    <if var="redirect">
      <meta http-equiv="refresh" content="5;${redirect-location}" />
    </if>
  </bind>
  <bind tag="page-body">
    <div class="message-page">
      <apply template="/message-span" />
      <if var="redirect">
        <span class="redirecting">Redirecting...</span>
      </if>
    </div>
  </bind>
</apply>
