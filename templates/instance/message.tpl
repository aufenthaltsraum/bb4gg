<apply template="/instance/base">
  <bind tag="instance-page-title">
    <page-name />
  </bind>
  <bind tag="instance-page-headers">
    <if var="redirect">
      <meta http-equiv="refresh" content="5;${redirect-location}" />
    </if>
  </bind>
  <bind tag="instance-page-body">
    <div class="message-page">
      <apply template="/message-span" />
      <if var="redirect">
        <span class="redirecting">Redirecting...</span>
      </if>
    </div>
  </bind>
</apply>
