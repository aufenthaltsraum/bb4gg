<apply template="/instance/base">
  <bind tag="instance-page-title">
    Main
  </bind>
  <bind tag="instance-page-headers">
    <style>
      .assignment-game {
        margin-bottom: 4px;
      }
      .dismiss-player-form {
        display: inline;
      }
      .dismiss-game {
        display: inline;
      }
      .player-list {
        display: inline;
        padding: 0;
        margin: 0;
      }
      .player-list > li {
        display: inline-block;
        border: 1px solid var(--border-color);
        border-radius: 4px;
        padding: 0 3px;
        margin: 0 0.05em 3px 0.05em;
        white-space: nowrap;
      }
      #players-lines-table th {
        text-align: right;
        vertical-align: text-top;
        white-space: nowrap;
      }
      .players-line-title {
        display: inline-block;
        margin: 1px 0 4px 0;
      }
      .player-status-form {
        display: inline;
      }
      .arrow-button, .cross-button {
        padding: 0;
        border: none;
        background: none;
      }
      .disabled {
        filter: grayscale(100%);
        color: grey;
      }
      .up-arrow:hover {
        filter: brightness(150%);
      }
      .down-arrow:hover, .double-down-arrow:hover, .cross-button:hover {
        filter: brightness(70%);
      }
      .game-name {
        font-weight: bold;
      }
      .specialGameName {
        font-style: italic;
      }
      #preferencesTable .ready {
        text-decoration: underline;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="/_static/tooltip.css" />
    <script type="module" src="/_static/tooltip.js" />
    <script type="module">
      import { setSubmitListeners } from '/_static/background_form.js';
      import { setUpWebSockets } from '/_static/websockets.js';
      // Needs to be done every time the contents are reset.
      setSubmitListeners();

      window.addEventListener('load', () => setUpWebSockets('MainChannel', (event) => {document.getElementById('mainContent').innerHTML = event.data; setSubmitListeners()}));
    </script>
  </bind>
  <bind tag="instance-page-body">
    <apply template="/instance/disconnected" />
    <div id="mainContent">
      <apply template="/instance/main/content" />
    </div>
  </bind>
</apply>
