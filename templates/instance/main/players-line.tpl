<tr class="players-line">
  <th>
    <span class="players-line-title"><players-line-title />:</span>
  </th>
  <td>
    <ul class="player-list">
      <players>
        <li class="form-wrap">
          <player-arrows>
            <value>
              <form class="player-status-form submit-background" action="/${instance-name}/player/modify/set-status" method="post">
                <input type="hidden" name="id" value="${player-id}" />
                <input type="hidden" name="status" value="${arrow-target-status}" />
                <button class="arrow-button ${arrow-direction}-arrow"><img src="/_static/arrow/${arrow-direction}.svg" alt="${arrow-direction}" /></button>
              </form>
            </value>
            <else />
            <form class="player-status-form submit-background" action="/${instance-name}/results/dismiss-player" method="post">
              <input type="hidden" name="id" value="${player-id}" />
              <button class="cross-button">&#x274C;</button>
            </form>
          </player-arrows>
          <a href="/${instance-name}/player/modify/?id=${player-id}"><apply template="player-span" /></a>
        </li>
      </players>
    </ul>
  </td>
</tr>
