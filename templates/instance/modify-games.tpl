<apply template="/instance/base">
  <bind tag="instance-page-title">
    Modify Games
  </bind>
  <bind tag="instance-page-headers">
    <style>
      input[type="number"] {
        width: 3em;
      }
      
      main {
        display: grid;
        grid-template-columns: 0fr 1fr;
      }
      
      #selected-game-container {
        white-space: nowrap;
      }
      
      #name-and-search {
        position: relative;
        display: inline-block;
      }
      
      #modifyGames\.gameName {
        width: 100%;
        box-sizing: border-box;
      }
      
      #search-results {
        /* This will be set by JS. */
        --content-height: 0px;
        --border-width: 1px;
        position: absolute;
        top: 100%;
        left: 0;
        width: calc(100% - 2 * var(--border-width));
        box-sizing: content-box;
        height: var(--content-height);
        max-height: 50vh;
        border: var(--border-width) solid black;
        border-radius: 3px;
        visibility: hidden;
        /* Without the following line errors occurr when clicking on this element (at least on Firefox); the focus moves faster than the click event is evaluated. */
        transition: visibility 0s 0.1s;
      }
      
      #name-and-search:focus-within #search-results:not([src="about:blank"]) {
        visibility: visible;
      }
      
      #selected-game-more-info {
        /* This will be set by JS. */
        --content-height: 0px;
        height: var(--content-height);
        background-color: lavender;
      }
      
      @media (min-width: 650px) {
        #selected-game-more-info {
          display: none;
          width: 450px;
          grid-column: 2;
          grid-row: 1 / 3;
          margin-left: 10px;
          padding: 4px 8px;
          border: 1px solid black;
          border-radius: 5px;
        }
        
        #selected-game-more-info:not([src="about:blank"]) {
          display: initial;
        }
        #new-game {
          display: grid;
          grid-template-columns: 1fr 1fr;
          grid-column-gap: 1rem;
        }
      }

      @media (max-width: 649px) {
        /* Otherwise "position: fixed" does not work for some reason */
        html, body {
          overflow: auto;
        }
        
        /* Otherwise the tooltip might block the lowest part of the site */
        body {
          padding-bottom: 100vh;
        }
        
        #selected-game-more-info {
          --padding-horizontal: 5px;
          visibility: hidden;
          /* Without the following line errors occurr when clicking on this element (at least on Firefox); the focus moves faster than the click event is evaluated. */
          transition: visibility 0s 0.1s;
          position: fixed;
          left: 0;
          bottom: 0;
          width: calc(100vw - 2 * var(--padding-horizontal));
          z-index: 1;
          border-top: 1px solid black;
          border-left: none;
          border-right: none;
          border-bottom: none;
          padding: 2px var(--padding-horizontal);
        }
        
        /* This way, the extra information unfortunately vanishes shortly after clicking on it; I don't see a pure CSS way around this. */
        #selected-game-container:focus-within ~ #selected-game-more-info:not([src="about:blank"]) {
          visibility: visible;
        }
      }
      
      #games-list-container {
        grid-column: 1;
      }
      
      .game-player-range, .game-recommended-player-range, .game-copies {
        padding: 0 5px;
      }
      
      .game-more-info {
        margin-left: 0.7em;
      }
    </style>
    <if var="modify">
      <script src="/_static/confirm.js" />
    </if>
    <link rel="stylesheet" type="text/css" href="/_static/bgg-tooltip.css" />
    <script src="/_static/bgg-tooltip.js" defer />
  </bind>
  <bind tag="instance-page-body">
    <main>
      <div id="selected-game-container">
        <if var="modify">
          <h2>Modify a Game</h2>
          <form id="delete-game-${selected-game-id}" action="/${instance-name}/games/modify/delete" method="post">
            <input type="hidden" name="id" value="${selected-game-id}" />
            <input type="button" onclick="confirmDeletion('game', '${selected-game-name}', ${selected-game-id})" value="Delete this game (${selected-game-name})" class="button delete" />
          </form>
          <else />
          <h2>Add a Game</h2>
        </if>

        <dfForm action="/${instance-name}/games/modify/?id=${selected-game-id}" autocomplete="off">
          <div id="new-game">
              <div><dfLabel ref="gameName">Name of the Game: </dfLabel></div>
              <div>
                <div id="name-and-search" style="width: calc(${max-game-name-length} * 1.2ch);">
                  <dfInputText ref="gameName" minlength="1" maxlength="${max-game-name-length}" autofocus />
                  <iframe id="search-results" src="about:blank" title="BoardGameGeek search results" />
                </div>
                <errors ref="gameName" />
              </div>
              <div><dfLabel ref="gameComment">Comment: </dfLabel></div>
              <div>
                <dfInputText ref="gameComment" maxlength="${max-game-comment-length}" style="width: calc(${max-game-comment-length} * 1.2ch);" />
                <errors ref="gameComment" />
              </div>
              <div>Number of Players (optional with BGG id):</div>
              <div>
                <dfInputText type="number" ref="min" min="1" max="${max-users}" />
                &#8211;
                <dfInputText type="number" ref="max" min="1" max="${max-users}" />
                <errors ref="min" />
                <errors ref="max" />
              </div>
              <div>Recommended Number of Players (optional):</div>
              <div>
                <dfInputText type="number" ref="recommendedMin" min="1" max="${max-users}" />
                &#8211;
                <dfInputText type="number" ref="recommendedMax" min="1" max="${max-users}" />
                <errors ref="recommendedMin" />
                <errors ref="recommendedMax" />
              </div>
              <div><dfLabel ref="bggID"><a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/">BoardGameGeek</a> identifier (or URL): </dfLabel></div>
              <div>
                <dfInputText ref="bggID" style="width: 8em;" />
                <errors ref="bggID" />
              </div>
              <div><dfLabel ref="maxCopies">Maximum number of simultaneous instances: </dfLabel></div>
              <div>
                <dfInputText type="number" ref="maxCopies" min="1" max="${max-users}" />
                <errors ref="maxCopies" />
              </div>
          </div>
          <errors ref="" />
          <p>
            <input id="submit-button" type="submit" value="Submit" class="button confirm" />
          </p>
        </dfForm>
      </div>
      
      <div id="games-list-container">
        <h2>List of Games</h2>
        <p>Click on a game to modify it or on the checkbox to toggle its availibility status (unavailable games are greyed out).</p>
        <table id="games-list">
          <thead>
            <tr>
              <th></th>
              <th colspan="2">Name (Comment)</th>
              <th>Players</th>
              <th>Recom.</th>
              <th>Copies</th>
            </tr>
          </thead>
          <tbody>
            <games>
              <tr class="availability-${game-availability}" id="game-${game-id}">
                <td>
                  <input type="checkbox" onclick="checkboxClicked(${game-id})" if-then="available=>checked" />
                </td>
                <td>
                  <a href="?id=${game-id}" class="game-name"><game-name /></a>
                  <game-comment><span class="game-comment">(<value />)</span></game-comment>
                </td>
                <td>
                  <apply template="/instance/game-info/more-info" />
                </td>
                <td class="game-player-range" style="text-align: center">
                  <game-player-range><apply template="range" /></game-player-range>
                </td>
                <td class="game-recommended-player-range" style="text-align: center">
                  <game-recommended-player-range><apply template="range" /></game-recommended-player-range>
                </td>
                <td class="game-copies" style="text-align: center"><game-copies /></td>
              </tr>
            </games>
          </tbody>
        </table>
      </div>
      
      <iframe id="selected-game-more-info" src="about:blank" title="BoardGameGeek data for selected game" />
    </main>
    
    <form id="toggle-game-form" action="/${instance-name}/games/modify/toggle" method="post">
        <input id="toggle-game-id" type="hidden" name="id" />
      </form>
    
    <script>
      function checkboxClicked(gameID) {
        document.getElementById('toggle-game-id').value = gameID;
        document.getElementById('toggle-game-form').submit();
      };
    </script>
    <script>
      function setSize(iframe) {
        const contentHeight = iframe.contentWindow.document.documentElement.scrollHeight;
        if(contentHeight > 0) {
          iframe.style.setProperty('--content-height', `${contentHeight}px`);
        } else {
          // Sometimes the "load" event fires before the content has rendered and its height is known
          setTimeout(setSize(iframe), 100);
        }
      };
      
      const selectedInfoIFrame = document.getElementById('selected-game-more-info');
      const bggIDField = document.getElementById('modifyGames.bggID');
      
      function showSelectedMoreInfo() {
        if(/^\d+$/.test(bggIDField.value)) {
          selectedInfoIFrame.setAttribute('src', `/_bgg?id=${bggIDField.value}`);
        }
        else {
          selectedInfoIFrame.setAttribute('src', "about:blank");
        }
      }
      
      // Do it now for the case that something was prefilled in the id field
      showSelectedMoreInfo();
      
      bggIDField.addEventListener('input', e => {
        const result = bggIDField.value.match(/^.*\/([0-9]+)(\/.*)?$/);
        if(result) {bggIDField.value = result[1];}
        showSelectedMoreInfo();
      });
      
      selectedInfoIFrame.addEventListener('load', e => {
        if(selectedInfoIFrame.getAttribute('src') != "about:blank") {
          setSize(selectedInfoIFrame);
        }
      });
      
      const nameField = document.getElementById('modifyGames.gameName');
      const resultsIFrame = document.getElementById('search-results');
      
      resultsIFrame.addEventListener('load', e => {
        if(resultsIFrame.getAttribute('src') != "about:blank") {
          setSize(resultsIFrame);
        }
      });
      
      function doSearch(query) {
        query = query.replaceAll(/[\$&\+,/:;=\?@#%]/g, '');
        if(query.length >= 3) {
          resultsIFrame.setAttribute('src', `/_bgg?type=boardgame,boardgameexpansion,videogame&max=50&query=${query}`);
        }
        else {
          resultsIFrame.setAttribute('src', "about:blank");
        }
      };
      
      let timeoutNameField = null;
      nameField.addEventListener('input', e => {
        if(timeoutNameField !== null) {
          clearTimeout(timeoutNameField);
        }
        timeoutNameField = setTimeout( () => {
          doSearch(nameField.value);
          timeoutNameField = null;
        }, 500);
      });
      
      nameField.addEventListener('focus', e => {
        // For the case that something was prefilled in the name field
        if(resultsIFrame.getAttribute('src') === "about:blank") {
          doSearch(nameField.value);
        }
      });
      
      const submitButton = document.getElementById("submit-button");
      window.addEventListener('message', e => {
        if(e.origin === location.origin && e.source === resultsIFrame.contentWindow && e.data['topic'] === "BGG game selected") {
          nameField.value = e.data['bgg-game-name'];
          bggIDField.value = e.data['bgg-game-id'];
          submitButton.focus();
          doSearch(nameField.value);
          showSelectedMoreInfo();
        }
      });
      
      nameField.addEventListener('keydown', e => {
        if(e.key === "ArrowDown" || e.key === "ArrowUp" || e.key === "Enter") {
          resultsIFrame.contentWindow.postMessage({'topic': "key press", 'key': e.key}, location.origin);
          e.preventDefault();
        }
      });
    </script>
  </bind>
</apply>
