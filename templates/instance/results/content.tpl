<if var="assignmentsPossible">
  <h2>
    Possible Assignments
    <if var="locked">
      (Locked until <apply template="/instance/locked-time" />)
    </if>
  </h2>
  <hr />
  <possible-assignments>
    Score: <assignment-average-score />, Minimum: <assignment-minimum-score /> -
    <form class="choose-assignment-form" action="/${instance-name}/results/choose" method="post">
      <input type="hidden" name="id" value="${assignment-id}" />
      <input type="submit" value="Choose this assignment" class="button inline confirm" />
    </form>
    <br />
    <assignment-games>
      <game-info />:
      <ul class="comma-list">
        <game-playing-players>
          <li><apply template="/tooltip"><bind tag="tooltip">Score: <player-score /></bind><span class="${player-status}"><apply template="player-span" /></span></apply></li>
        </game-playing-players>
      </ul>
      <br />
    </assignment-games>
    <hr />
  </possible-assignments>
  <if var="stillComputing"><h3>Still computing...</h3></if>

  <else />

  <h3>
    <if var="noActivePlayers">
      There are no currently active players.
    </if>
    <if var="playersNotYetReady">
      The player<if var="plural">s</if>
      <ul class="comma-list">
        <error-players>
          <li><apply template="player-span" /></li>
        </error-players>
      </ul>
      <if var="plural">are<else />is</if>
      not ready yet.
    </if>
    <if var="playersWithoutPreference">
      The player<if var="plural">s</if>
      <error-players><apply template="player-span" /></error-players>
      <if var="plural">have<else />has</if>
      no active preference.
    </if>
    <if var="needToRecompute">
      Results need to be recomputed. <noscript>Please refresh the page.</noscript>
    </if>
    <if var="notRunning">
      Computation not (yet) running. <noscript>Please refresh the page.</noscript>
    </if>
    <if var="noPossibleAssignments">
      There are no possible assignments.
    </if>
  </h3>
</if>
