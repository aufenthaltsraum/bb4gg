<!DOCTYPE html>
<html lang="en">
  <head>
    <title><page-title /> | BB4GG</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />
    <style>
      :root {
        --background-color: white;
        --color: black;
        --error-color: red;
        --link-color: #2980b9;
        --border-color: silver;
        --hover-color: lightblue;
        --delete-hover-color: crimson;
        --confirm-hover-color: #27bb27;
        --unavailable-color: darkgrey;
        --tooltip-color: #eee;
      }

      @media (prefers-color-scheme: dark) {
          :root {
            --background-color: #111;
            --color: #ddd;
            --link-color: dodgerblue;
            --hover-color: darkcyan;
            --delete-hover-color: orangered;
            --confirm-hover-color: lawngreen;
            --unavailable-color: #555;
            --tooltip-color: #444;
          }
          input, button { background-color: inherit; color: inherit; }
      }

      html { background-color: var(--background-color); color: var(--color);}
      .availability-False {color: var(--unavailable-color); font-style: italic}
      a {text-decoration: inherit; color: var(--link-color);}
      a:hover {text-decoration: underline}
      .error {color: red}
      .message-page {
        display: block;
        margin-left: 1em;
        margin-top: 1em;
      }
      .button {
        border: 1px solid var(--border-color);
        border-radius: 3px;
        padding: 7px 14px;
      }
      .button:hover {
        border: 1px solid var(--hover-color);
      }
      .button.inline {
        padding: 3px 6px;
      }
      .button.delete:hover {
        color: var(--delete-hover-color);
      }
      .button.confirm:hover {
        color: var(--confirm-hover-color);
      }
      .range-glyph {
        padding: 0 0.25em;
      }
      
      .game-comment {
        font-size: smaller;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="/_static/comma-list.css" />
    <page-headers />
  </head>
  <body>
    <page-body />
  </body>
</html>
