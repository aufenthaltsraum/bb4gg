<html>
  <head>
    <title>BoardGameGeek data for "<bgg-game-name />"</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />
    <style>
      body {
        text-align: center;
        margin: 0;
      }
      
      .range-glyph {
        padding: 0 0.25em;
      }
      
      a {
        text-decoration: inherit;
        color: #2980b9;
      }
      a:hover {
        text-decoration: underline;
      }
      
      .error {
        color: crimson;
      }
      
      .number {
        font-weight: bold;
      }
      
      .row.flex {
        display: flex;
      }
      .label {
        font-size: 0.7em;
      }
      .item {
        border: 1px solid lightgrey;
        padding: 3px;
        border-radius: 5px;
      }
      
      .list {
        font-size: 0.9em;
        margin: 0;
        padding: 0;
        display: flex;
        flex-wrap: wrap;
        row-gap: 3px;
        column-gap: 2px;
      }
      .list .element {
        border: 1px solid lightgrey;
        padding: 0 2px;
        border-radius: 3px;
        display: inline-block;
      }
      
      header {
        margin-bottom: 5px;
      }
      
      #name {
        font-size: 1.5em;
        line-height: 1.1;
        margin: 0;
      }

      #type-and-year {
        font-size: 0.7em;
      }
      
      #image {
        display: inline-block;
      }
      #image a {
        display: inline-block;
      }
      
      #rating {
        color: white;
      }
      #rating .number {
        font-size: 1.3em;
      }
      #weight {
        color: white;
      }
      #weight .number {
        font-size: 1.3em;
      }

      #info {
        align-items: center;
      }
      
      #players {
        border-spacing: 0;
        font-size: 1em;
      }
      #players td {
        vertical-align: baseline;
        padding: 0;
      }
      #players .label {
        text-align: right;
      }
      #players .number {
        padding-left: 0.2em;
      }
      #players-recommended {
        font-size: 0.7em;
      }
      #players-recommended .label {
        font-size: inherit;
      }
      
      .link-type {
        text-align: left;
      }
      
      footer {
        margin-top: 5px;
      }
      
      #source {
        float: right;
        font-size: 0.7em;
      }
      
      @media (max-width: 249px) {
        body {
          overflow-x: hidden;
          width: 220px;
        }
        
        .row {
          padding: 0 5px;
        }
        .row.flex {
          justify-content: space-between;
        }
        
        #image {
          margin-bottom: 5px;
        }
        
        #stats {
          align-items: flex-end;
          margin-bottom: 5px;
        }
        
        #info {
          margin-bottom: 5px;
        }
      }
      
      @media (min-width: 250px) {
        body {
          font-size: 0.9em;
        }
        
        main {
          display: grid;
          grid-template-columns: repeat(2, 1fr);
          grid-gap: 5px;
        }
        
        .row.flex {
          justify-content: space-around;
        }
        
        #image {
          grid-column: 1;
          grid-row: 1 / 3;
        }
        #image img {
          max-width: 100%;
        }
        
        #stats {
          grid-column: 2;
          align-items: center;
        }
        #info {
          grid-column: 2;
          flex-wrap: wrap;
          align-items: center;
          row-gap: 3px;
        }
        
        .link-type {
          grid-column: 1 / 3;
        }
      }
    </style>
    <link rel="stylesheet" type="text/css" href="/_static/comma-list.css" />
  </head>
  <body>
    <any-errors>
      <div class="error"><error-message /></div>
      <else />
      <header>
        <h1 id="name">
          <a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/boardgame/${bgg-game-id}">
            <bgg-game-name />
          </a>
        </h1>
        <div id="type-and-year">
          (<bgg-game-type /><bgg-game-year>, <value /></bgg-game-year>)
        </div>
      </header>
      <main>
        <div id="image">
          <a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/boardgame/${bgg-game-id}">
            <bgg-game-thumbnail>
             <img src="${value}" alt="${bgg-game-name}" />
             <else />
             <img src="https://cf.geekdo-images.com/zxVVmggfpHJpmnJY9j-k1w__thumb/img/Tse35rOD2Z8Pv9EOUj4TfeMuNew=/fit-in/200x150/filters:strip_icc()/pic1657689.jpg" alt="No image available" />
            </bgg-game-thumbnail>
          </a>
        </div>
        <div id="stats" class="row flex">
          <span id="rating" class="item" style="background-color: ${bgg-game-rating-color}">
            <div class="number">
              <bgg-game-rating><value /><else />&#8212;</bgg-game-rating>
            </div>
            <div class="label">
              Rating
            </div>
          </span>
          <span id="rank" class="item">
            <div class="number">
              <bgg-game-rank><value /><else />&#8212;</bgg-game-rank>
            </div>
            <div class="label">
              Rank
            </div>
          </span>
          <span id="weight" class="item" style="background-color: ${bgg-game-weight-color}">
            <div class="number">
              <bgg-game-weight><value /> / 5<else />&#8212;</bgg-game-weight>
            </div>
            <div class="label">
              Complexity
            </div>
          </span>
        </div>
        <div id="info" class="row flex">
          <table id="players" class="item">
            <tbody>
              <tr id="players-official">
                <td class="label">
                  Players:
                </td>
                <td class="number">
                  <bgg-game-players><apply template="range" /></bgg-game-players>
                </td>
              </tr>
              <bgg-game-players-recommended>
                <tr id="players-recommended">
                  <td class="label">
                    (Best:
                  </td>
                  <td>
                    <ul class="comma-list number"><value><li><apply template="range" /></li></value></ul>)
                  </td>
                </tr>
              </bgg-game-players-recommended>
            </tbody>
          </table>
          <span id="length" class="item">
            <div class="number">
              <bgg-game-length><apply template="range" /></bgg-game-length> min
            </div>
            <div class="label">
              Length
            </div>
          </span>
        </div>
        <bgg-link-types>
          <div class="row link-type">
            <div class="label">
              <bgg-link-type-label />
            </div>
            <ul class="list">
              <bgg-links type="boardgamecategory">
                <li class="element">
                  <a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/boardgamecategory/${bgg-link-id}"><bgg-link-name /></a>
                </li>
              </bgg-links>
            </ul>
          </div>
        </bgg-link-types>
      </main>
    </any-errors>
    <footer>
      <small id="source">
        Data from <a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/">BoardGameGeek</a>
      </small>
    </footer>
  </body>
</html>
