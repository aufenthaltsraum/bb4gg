<html>
  <head>
    <title>BoardGameGeek search results for "<bgg-search-query />"</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />
    <style>
      :root {
        --background-color: white;
      }
      
      html {
        overflow-x: hidden;
        overflow-y: auto;
        overscroll-behavior: contain;
      }
      
      body {
        margin: 0;
        background-color: var(--background-color);
        font-size: 0.8em;
      }
      
      #no-results-message, .error {
        width: 100%;
        margin: 5px 0;
        text-align: center;
      }
      
      #no-results-message {
        font-style: italic;
      }
      
      .error {
        color: crimson;
      }
      
      #results {
        margin: 0;
        padding: 0;
        display: flex;
        flex-direction: column;
      }
      
      .result {
        display: block;
      }
      
      .result-button {
        display: block;
        margin: 0;
        padding: 2px 4px;
        border: none;
        text-align: left;
        width: 100%;
        cursor: pointer;
        background-color: inherit;
      }
      
      .result-button:hover, .result-button:active {
        background-color: lavender;
      }
      
      /* Selected using the arrow keys */
      .result.selected .result-button {
        background-color: thistle;
      }
      
      .year {
        font-weight: lighter;
      }
      
      footer {
        position: sticky;
        bottom: 0;
        left: 0;
      }
      
      #source {
        display: inline-block;
        background-color: var(--background-color);
        border-top: 1px solid black;
        border-right: 1px solid black;
        border-radius: 0 3px 0 0;
        padding: 1px 3px 0 3px;
      }
    </style>
  </head>
  <body>
    <any-errors>
      <div class="error"><error-message /></div>
      <else />
      <main>
        <if var="noResults">
          <div id="no-results-message">No results</div>
        </if>
        <menu id="results">
          <bgg-search-results>
            <li class="result" data-id="${bgg-search-result-id}" data-name="${bgg-search-result-name}">
              <button class="result-button">
                <span class="name"><bgg-search-result-name /></span>
                <bgg-search-result-year>
                  <span class="year">(<value />)</span>
                </bgg-search-result-year>
              </button>
            </li>
          </bgg-search-results>
        </menu>
      </main>
    </any-errors>
    <footer id="footer">
      <small id="source">
        Data from <a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/">BoardGameGeek</a>
      </small>
    </footer>
  </body>
  <script>
    document.querySelectorAll('.result-button').forEach(el => {
      el.addEventListener('click', e => {
        window.parent.postMessage(
          { 'topic': "BGG game selected"
          , 'bgg-game-id': el.parentElement.getAttribute('data-id')
          , 'bgg-game-name': el.parentElement.getAttribute('data-name')
          }, location.origin
        );
      });
    });
    
    const results = document.getElementById('results');
    let selected = null;
    window.addEventListener('message', e => {
      if(e.origin === location.origin && e.source === window.parent && e.data['topic'] === "key press") {
        let key = e.data['key'];

        if(key === "Enter") {
          if(selected) {selected.querySelector('.result-button').click();}
        }
        else if(key === "ArrowDown" || key === "ArrowUp") {
          if(selected) {selected.classList.remove('selected');}
          
          if(key === "ArrowDown") {
            if(!selected) {selected = results.firstElementChild;}
            else if(selected.nextElementSibling) {selected = selected.nextElementSibling;}
          }
          else if(key === "ArrowUp") {
            if(selected) {selected = selected.previousElementSibling;}
          }
          
          if(selected) {
            selected.classList.add('selected');
            
            // Keep selected item in view
            let rect = selected.getBoundingClientRect();
            const footerHeight = document.getElementById('footer').offsetHeight;
            if(rect.bottom > document.body.clientHeight - footerHeight) {
              window.scrollBy(0, rect.bottom - document.body.clientHeight + footerHeight);
            }
            else if(rect.top < 0) {
              window.scrollBy(0, rect.top);
            }
          }
        }
      }
    });
  </script>
</html>
