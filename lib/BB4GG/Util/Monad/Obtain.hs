{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Util.Monad.Obtain
  ( MonadObtain (..)
  , obtains
  , obtaining
  ) where

import Control.Lens (Getting, view)
import Control.Monad.Trans (MonadTrans, lift)

-- | This class provides a function to extract a value of a specified type from the monad.
-- This is more general than 'MonadReader'.
class Monad m => MonadObtain o m | m -> o where
  obtain :: m o

instance
  {-# OVERLAPPABLE #-}
  (MonadObtain o m, MonadTrans t, Monad (t m))
  => MonadObtain o (t m)
  where
  obtain = lift obtain

-- | Utility function; maps the provided function over 'obtain'.
obtains :: MonadObtain o m => (o -> a) -> m a
obtains f = fmap f obtain

-- | Utility function for accessing the value provided via 'obtain' with a lens.
obtaining :: MonadObtain o m => Getting a o a -> m a
obtaining g = obtains $ view g
