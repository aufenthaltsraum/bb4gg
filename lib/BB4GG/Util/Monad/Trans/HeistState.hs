{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Util.Monad.Trans.HeistState
  ( module BB4GG.Util.Monad.HeistState
  , HeistStateT (..)
  , initBB4GGHeist
  , runHeistStateT
  ) where

import BB4GG.Util.Monad.HeistState

import Control.Applicative (Alternative)
import Control.Concurrent.Forkable (ForkableMonad)
import Control.Lens ((&), (.~))
import Control.Monad (MonadPlus)
import Control.Monad.Base (MonadBase)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Morph (MFunctor)
import Control.Monad.Reader (ReaderT, ask, runReaderT)
import Control.Monad.Trans (MonadTrans)
import Control.Monad.Trans.Control (MonadBaseControl, MonadTransControl)
import Heist
  ( HeistState
  , defaultLoadTimeSplices
  , emptyHeistConfig
  , hcNamespace
  , hcSpliceConfig
  , hcTemplateLocations
  , initHeist
  , loadTemplates
  , scLoadTimeSplices
  )
import Snap.Core (MonadSnap)
import System.Exit (die)

-- | A synonym for 'ReaderT (HeistState BB4GGHeistM)' which is used as the outermost transformer of 'BB4GGT' and 'BB4GGInstanceT'.
--
-- Not calling this just 'HeistT' because that name is already used in the Heist library.
newtype HeistStateT m a = HeistStateT
  { readerHT :: ReaderT (HeistState BB4GGHeistM) m a
  }
  deriving
    ( Alternative
    , Applicative
    , ForkableMonad
    , Functor
    , MFunctor
    , Monad
    , MonadBase b
    , MonadBaseControl b
    , MonadIO
    , MonadPlus
    , MonadSnap
    , MonadTrans
    , MonadTransControl
    )

instance MonadIO m => MonadHeistState (HeistStateT m) where
  obtainHeistState = HeistStateT ask

-- | Run a 'HeistStateT'.
runHeistStateT :: HeistStateT m a -> HeistState BB4GGHeistM -> m a
runHeistStateT = runReaderT . readerHT

-- | Initialize the Heist state that will be used throughout the program.
initBB4GGHeist
  :: (Monad n, MonadIO m)
  => FilePath
  -- ^ Path to the directory containing the HTML templates.
  -> m (HeistState n)
initBB4GGHeist p =
  -- For some reason, there doesn't seem to be a reasonable default Heist config and we have to construct one by hand...
  liftIO $
    do
      let spliceConfig =
            mempty & scLoadTimeSplices .~ defaultLoadTimeSplices
          heistConfig =
            emptyHeistConfig
              & hcSpliceConfig .~ spliceConfig
              & hcNamespace .~ ""
              & hcTemplateLocations .~ [loadTemplates p]
      heistResult <- initHeist heistConfig
      case heistResult of
        Left es -> die $ "Can't load HTML templates:\n\n" ++ unlines es
        Right hs -> return hs
