{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

module BB4GG.Util.Cache
  ( Cache
  , entriesC
  , insertMultiple
  , isStale
  , mkCache
  , valueCE
  ) where

import Control.Lens (makeLenses, over, set, view, (^.))
import Data.Time (NominalDiffTime, UTCTime, addUTCTime)

import Data.Map.Strict qualified as M

-- | One entry of the cache of type 'a'.
data CacheEntry a = CacheEntry
  { _valueCE :: Maybe a
  -- ^ We also cache the non-existence of an entry.
  , _timeCE :: UTCTime
  }
  deriving (Show, Eq, Ord)

makeLenses ''CacheEntry

-- | The cache with entries of type 'a' indexed by type 'ix'.
data Cache ix a = Cache
  { _maxNumC :: Maybe Int
  -- ^ Maximum number of items allowed in the cache.
  , _timeUntilUpdateC :: NominalDiffTime
  -- ^ The time after which to update an entry.
  , _byTimeC :: M.Map UTCTime ix
  -- ^ Entry indices by insertion time.
  , _entriesC :: M.Map ix (CacheEntry a)
  -- ^ Cache entries.
  }
  deriving (Show)

makeLenses ''Cache

-- | Create a cache.
mkCache
  :: Maybe Int
  -- ^ Maximum number of items allowed in the cache.
  -> NominalDiffTime
  -- ^ The time after which to update an entry.
  -> Cache ix a
mkCache maxNum timeUntilUpdate =
  Cache
    { _maxNumC = maxNum
    , _timeUntilUpdateC = timeUntilUpdate
    , _byTimeC = M.empty
    , _entriesC = M.empty
    }

-- | Removes the oldest entries if it has more then the maximum number of entries.
clean :: Ord ix => Cache ix a -> Cache ix a
clean c@Cache {_maxNumC = Just maxNum} =
  case M.minViewWithKey $ c ^. byTimeC of
    Just ((_, ix), byTime')
      | M.size (c ^. entriesC) - maxNum > 0 ->
          clean $ over entriesC (M.delete ix) $ set byTimeC byTime' c
    _ -> c
clean c = c

-- | Delete one entry from the cache.
delete :: Ord ix => ix -> Cache ix a -> Cache ix a
delete ix c =
  case M.lookup ix $ c ^. entriesC of
    Nothing -> c
    Just ce ->
      over byTimeC (M.delete $ ce ^. timeCE) $ over entriesC (M.delete ix) c

-- | Insert one element into the cache (potentially cleaning an old entry).
insert :: Ord ix => UTCTime -> ix -> Maybe a -> Cache ix a -> Cache ix a
insert time ix ma c
  | maybe True ((< time) . view timeCE) $ (c ^. entriesC) M.!? ix =
      clean
        . over byTimeC (M.insert time ix)
        . over entriesC (M.insert ix $ CacheEntry ma time)
        $ delete ix c
insert _ _ _ c = c

-- | Insert multiple elements into the cache (potentially cleaning old entries).
insertMultiple
  :: Ord ix => UTCTime -> M.Map ix (Maybe a) -> Cache ix a -> Cache ix a
insertMultiple time entries cache =
  M.foldlWithKey' (\c ix ma -> insert time ix ma c) cache entries

isStale :: Cache ix a -> UTCTime -> CacheEntry a -> Bool
isStale cache now entry = now >= (cache ^. timeUntilUpdateC) `addUTCTime` (entry ^. timeCE)
