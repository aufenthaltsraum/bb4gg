module BB4GG.Web (site) where

import BB4GG.Core

import BB4GG.Web.API (dumpInstanceState)
import BB4GG.Web.Page.BGGSnippet (bggSnippet)
import BB4GG.Web.Page.DoActionByID
  ( clearPlayersAction
  , deleteGameAction
  , deleteInstanceAction
  , deletePlayerAction
  , dismissAllAction
  , dismissGameAction
  , dismissPlayerAction
  , doInstanceActionByIDPage
  , doInstanceActionByParamPage
  , doInstanceActionByParamsPage
  , doStateActionByIDPage
  , importFromBGGCollectionAction
  , setCurrentAssignmentAction
  , setPlayerStatusAction
  , toggleGameAction
  , updateWithBGGAction
  )
import BB4GG.Web.Page.Instance.Configure (configureInstancePage)
import BB4GG.Web.Page.Instance.Games (modifyGamesPage)
import BB4GG.Web.Page.Instance.Main (instanceMainPage)
import BB4GG.Web.Page.Instance.Player (modifyPlayerPage)
import BB4GG.Web.Page.Instance.Results (resultsPage)
import BB4GG.Web.Page.Main (mainPage)
import BB4GG.Web.Util.Splices (errorPageSplices)

import Control.Arrow ((&&&))
import Control.Lens ((^.))
import Control.Monad (void)
import Network.WebSockets.Snap (runWebSocketsSnap)
import Snap.Core (MonadSnap, addHeader, modifyResponse, route, setResponseCode)
import Snap.Util.FileServe (serveDirectory, serveFile)
import System.FilePath ((</>))

import Data.ByteString.UTF8 qualified as BS
import Data.Set qualified as S
import Data.Vector.Sized qualified as V

-- | Create a Snap site using the given state variable, mapping URLs to corresponding actions.
site :: MonadBB4GG MonadSnap m => m ()
site =
  do
    staticPath <- (</> "static") . (^. dirsBC . bb4ggDC) <$> getConfig
    route
      . ( ++
            [
              ( ""
              , mainPage
              )
            ,
              ( "_instances/delete"
              , doStateActionByIDPage deleteInstanceAction
              )
            ,
              ( "/robots.txt"
              , serveFile (staticPath </> "robots.txt")
              )
            ,
              ( "/_static"
              , do
                  modifyResponse (addHeader "Cache-Control" "max-age=86400")
                  serveDirectory staticPath
              )
            ,
              ( "/_bgg"
              , bggSnippet
              )
            ,
              ( ":_"
              , respond404 "/"
              )
            ]
        )
      -- Here it's important that the ByteString passed to the route function
      -- is UTF8-encoded.
      . map (BS.fromString &&& (void . runInstance instanceRoute))
      . S.toList
      =<< getInstanceNames

instanceRoute :: MonadBB4GGInstance LockIMax MonadSnap m => m ()
instanceRoute =
  route
    [
      ( ""
      , instanceMainPage
      )
    ,
      ( "websockets"
      , runWebSocketsSnap =<< serverAppWS
      )
    ,
      ( "clear"
      , doInstanceActionByIDPage clearPlayersAction
      )
    ,
      ( "games/modify"
      , modifyGamesPage
      )
    ,
      ( "games/modify/delete"
      , doInstanceActionByIDPage deleteGameAction
      )
    ,
      ( "games/modify/toggle"
      , doInstanceActionByIDPage toggleGameAction
      )
    ,
      ( "player/modify"
      , modifyPlayerPage
      )
    ,
      ( "player/modify/delete"
      , doInstanceActionByIDPage deletePlayerAction
      )
    ,
      ( "player/modify/set-status"
      , doInstanceActionByIDPage setPlayerStatusAction
      )
    ,
      ( "results"
      , resultsPage
      )
    ,
      ( "results/choose"
      , doInstanceActionByIDPage setCurrentAssignmentAction
      )
    ,
      ( "results/dismiss-all"
      , doInstanceActionByIDPage dismissAllAction
      )
    ,
      ( "results/dismiss"
      , doInstanceActionByIDPage dismissGameAction
      )
    ,
      ( "results/dismiss-player"
      , doInstanceActionByIDPage dismissPlayerAction
      )
    ,
      ( "configure"
      , configureInstancePage
      )
    ,
      ( "api/dump"
      , dumpInstanceState
      )
    ,
      ( "update-with-bgg"
      , doInstanceActionByParamPage
          "type"
          (mapM readMaybeBGGThingType . words)
          updateWithBGGAction
      )
    ,
      ( "import-from-bgg-collection"
      , doInstanceActionByParamsPage
          (V.fromTuple ("type", "user") :: V.Vector 2 BS.ByteString)
          (\v -> fmap (,V.last v) . readMaybeBGGThingType $ V.head v)
          importFromBGGCollectionAction
      )
    ,
      ( ":_"
      , respond404 "/instance/"
      )
    ]

respond404 :: (MonadHeistState m, MonadSnap m) => BS.ByteString -> m ()
respond404 prefix =
  do
    modifyResponse $ setResponseCode 404
    respondWithSplices (prefix <> "message") $
      errorPageSplices "404 - page not found" Nothing
