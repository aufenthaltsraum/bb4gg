{- HLINT ignore "Reduce duplication" -}
{-# LANGUAGE QuantifiedConstraints #-}

module BB4GG.Web.Page.DoActionByID
  ( clearPlayersAction
  , deleteGameAction
  , deleteInstanceAction
  , deletePlayerAction
  , dismissAllAction
  , dismissGameAction
  , dismissPlayerAction
  , doInstanceActionByIDPage
  , doInstanceActionByParamPage
  , doInstanceActionByParamsPage
  , doStateActionByIDPage
  , importFromBGGCollectionAction
  , setCurrentAssignmentAction
  , setPlayerStatusAction
  , toggleGameAction
  , updateWithBGGAction
  ) where

import BB4GG.Core

import BB4GG.BGG.Extract (importBGGIDs, updateGamesWithBGG)
import BB4GG.Core.Computation.Handling (resetComputation)
import BB4GG.Types.Util (readComputation, readMaybePlayerStatus, waitingPlayers)
import BB4GG.Util (batch)
import BB4GG.Util.Monad.Obtain (obtaining)
import BB4GG.Util.Monad.Unlockable (runUnlockedOrNotVoid, runUnlockedVoid)
import BB4GG.Web.Util.Splices (MsgType (Info), Path, errorPageSplices, messagePageSplices)
import BB4GG.Web.Websockets.Broadcast (broadcastAssignmentChosen, broadcastMain)

import Control.Concurrent.Forkable (ForkableMonad (forkIO))
import Control.Lens
  ( assign
  , at
  , indices
  , itraversed
  , ix
  , modifying
  , preuse
  , set
  , use
  , view
  , (^.)
  , _2
  )
import Control.Monad (join, void, when)
import Control.Monad.Except (ExceptT, MonadError, runExceptT, throwError)
import Control.Monad.Extra (ifM, unlessM)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (MonadReader, ReaderT, ask, runReaderT)
import Control.Monad.State (gets)
import Data.Maybe (isJust)
import Data.Time (getCurrentTime)
import Snap.Core (Method (..), MonadSnap, getPostParam, method, redirect)
import Text.Read (readMaybe)

import Data.ByteString.UTF8 qualified as BS
import Data.Map.Strict qualified as M
import Data.Set qualified as S
import Data.Vector.Sized qualified as V

-- | This type describes an action that can be executed by visiting a page.
-- @Maybe b@ is a parameter that was given as part of the url and then parsed to something of type @b@.
-- If this did not work, a @Nothing@ is passed.
-- @m@ is the inner monad everything lives in.
-- When there is an error it throws an error message and potentially redirects to another page via @ExceptT (String, Maybe Path)@.
-- The returned @(Maybe String, Path)@ consists of a potential message to show and the page the user should be redirected afterwards if the action was succesful.
type Action b m = ReaderT (Maybe b) (ExceptT (String, Maybe Path) m) (Maybe String, Path)

-- | Ask the MonadReader, throw an error if it is 'Nothing'.
askParsed
  :: (MonadReader (Maybe b) m, MonadError (String, Maybe Path) m)
  => String
  -> m b
askParsed name =
  maybe (throwError ("Could not parse " ++ name, Nothing)) return =<< ask

-- | Executes an 'Action' given its inputs.
runAction :: Action b m -> Maybe b -> m (Either (String, Maybe Path) (Maybe String, Path))
runAction action mB = runExceptT $ runReaderT action mB

-- | The page belonging to an 'Action' that uses multiple parameters.
doInstanceActionByParamsPage
  :: MonadBB4GGInstance LockIMax MonadSnap m
  => V.Vector n BS.ByteString
  -- ^ The POST parameters to use.
  -> (V.Vector n String -> Maybe a)
  -- ^ The function that parses the parameters.
  -> Action a m
  -- ^ The 'Action' to execute.
  -> m ()
doInstanceActionByParamsPage params parse action = method POST $
  do
    mInputsBS <- sequence <$> traverse getPostParam params
    let mParsed = parse . fmap BS.toString =<< mInputsBS
    result <- runAction action mParsed
    case result of
      Left (err, mRedirect) ->
        respondWithSplices "/instance/message" $ errorPageSplices err mRedirect
      Right success ->
        do
          saveInstanceStateUnlocked
          case success of
            (Just msg, redirectLocation) ->
              respondWithSplices "/instance/message" $ messagePageSplices Info msg (Just redirectLocation)
            (Nothing, redirectLocation) ->
              redirect redirectLocation

-- | The page belonging to an 'Action' that uses a single parameter.
doInstanceActionByParamPage
  :: MonadBB4GGInstance LockIMax MonadSnap m
  => BS.ByteString
  -- ^ The POST parameter to use.
  -> (String -> Maybe a)
  -- ^ The function that parses the parameter.
  -> Action a m
  -- ^ The 'Action' to execute.
  -> m ()
doInstanceActionByParamPage param parse =
  doInstanceActionByParamsPage (V.singleton param) (parse . V.head)

-- | The page belonging to an 'Action' on an instance that reads from the parameter "id".
doInstanceActionByIDPage
  :: (MonadBB4GGInstance LockIMax MonadSnap m, Read a)
  => Action a m
  -> m ()
doInstanceActionByIDPage = doInstanceActionByParamPage "id" readMaybe

-- | The page belonging to an 'Action' on the state.
doStateActionByIDPage :: MonadBB4GG MonadSnap m => Action InstanceName m -> m ()
doStateActionByIDPage action =
  method POST $
    do
      idBS <- getPostParam "id"
      let mID = fmap BS.toString idBS
      result <- runAction action mID
      case result of
        Left (err, mRedirect) ->
          respondWithSplices "/message" $ errorPageSplices err mRedirect
        Right (Just msg, redirectLocation) ->
          respondWithSplices "/message" $
            messagePageSplices
              Info
              msg
              (Just redirectLocation)
        Right (Nothing, redirectLocation) ->
          redirect
            redirectLocation

-- | The 'Action' of deleting the 'Instance' of some name.
-- The corresponding file on disk is *not* deleted, which is on purpose, to help with accidental deletions.
deleteInstanceAction :: MonadBB4GG b m => Action InstanceName m
deleteInstanceAction =
  do
    name <- askParsed "instance name"
    unlessM (runUnlocked' $ deleteInstance name) $
      throwError ("Instance not found", Nothing)
    return (Nothing, "/")

-- | The 'Action' of setting the 'PlayerStatus' of a player.
setPlayerStatusAction
  :: MonadBB4GGInstance LockISt MonadSnap m => Action PlayerID m
setPlayerStatusAction =
  do
    pID <- askParsed "player ID"
    statusStr <-
      maybe (throwError ("No status provided", Nothing)) (return . BS.toString)
        =<< getPostParam "status"
    status' <-
      maybe (throwError ("Unknown status", Nothing)) return $
        readMaybePlayerStatus statusStr
    join $
      runUnlocked @LockISt $
        do
          players <- use playersIS
          if M.member pID players
            then do
              now <- liftIO getCurrentTime
              assign
                (playersIS . ix pID . valueStampedTS now . statusPD)
                status'
              resetComputation
              return $ return ()
            else return $ throwError ("Player not found", Nothing)
    broadcastMain
    return (Nothing, "../..")

-- | The 'Action' of deleting the 'Player' belonging to some 'PlayerID'.
deletePlayerAction :: MonadBB4GGInstance LockISt c m => Action PlayerID m
deletePlayerAction =
  do
    pID <- askParsed "player ID"
    () <- join $
      runUnlocked @LockISt $
        do
          players <- use playersIS
          if M.member pID players
            then do
              modifying playersIS $ M.delete pID
              resetComputation
              return $ return ()
            else return $ throwError ("Player not found", Nothing)
    broadcastMain
    return (Nothing, "../..")

-- | The 'Action' of deleting all 'Players'.
clearPlayersAction :: MonadBB4GGInstance LockISt c m => Action () m
clearPlayersAction =
  do
    () <- runUnlocked @LockISt $
      do
        assign playersIS M.empty
        resetComputation
    broadcastMain
    return (Nothing, ".")

-- | The 'Action' of deleting the game belonging to some 'GameID'.
deleteGameAction :: MonadBB4GGInstance LockISt c m => Action NormalGameID m
deleteGameAction =
  do
    gID <- askParsed "game ID"
    () <- join $
      runUnlocked @LockISt $
        do
          games <- use gamesIS
          if M.member gID games
            then do
              batch $
                do
                  modifying gamesIS $ M.delete gID
                  modifying
                    (playersIS . traverse . valueUnstampedTS . prefPD)
                    . M.delete
                    $ NormalGID gID
              resetComputation
              return $ return ()
            else return $ throwError ("Game not found", Nothing)
    broadcastMain
    return (Nothing, ".")

-- | The 'Action' of toggling between a game being available and not.
toggleGameAction :: MonadBB4GGInstance LockISt c m => Action NormalGameID m
toggleGameAction =
  do
    gID <- askParsed "game ID"
    () <- join $
      runUnlocked @LockISt $
        do
          games <- use gamesIS
          if M.member gID games
            then do
              modifying (gamesIS . ix gID . isAvailableGD) not
              resetComputation
              return $ return ()
            else return $ throwError ("Game not found", Nothing)
    broadcastMain
    return (Nothing, "./#game-" <> BS.fromString (show gID))

-- | The 'Action' of setting the current 'Assignment' to the one with the given index.
setCurrentAssignmentAction :: MonadBB4GGInstance LockISt c m => Action Int m
setCurrentAssignmentAction =
  do
    i <- askParsed "assignment ID"
    () <- join $
      runUnlocked @LockISt $
        do
          mComp <- readComputation =<< use computationStatusIS
          case (M.lookup i . view computedAssignmentsC) =<< mComp of
            Just (assig, _) ->
              do
                now <- liftIO getCurrentTime
                batch $
                  do
                    modifying
                      ( playersIS
                          . itraversed
                          . indices (\pID -> any (S.member pID . snd) assig)
                      )
                      (set (valueStampedTS now . statusPD) WantsToPlayPS)
                    let playing =
                          M.filter
                            ((/= SpecialGID NotPlayingSG) . fst)
                            assig
                    modifying
                      (currentAssigIS . valueStampedTS now)
                      (<> playing)
                () <- broadcastAssignmentChosen
                resetComputation
                return $ return ()
            Nothing ->
              do
                err <- gets errorMsg
                return $ throwError err
    broadcastMain
    return (Nothing, "..")
  where
    errorMsg state
      | M.null (waitingPlayers state)
          && not (M.null $ state ^. currentAssigIS . valueTS) =
          ( "An assignment has already been chosen. \
            \You will be redirected to the main page...\
            \"
          , Just ".."
          )
      | otherwise =
          ( "The player preferences, games, and/or the chosen assignment have \
            \changed since computing the assignments. \
            \You will be redirected to the new results...\
            \"
          , Just "."
          )

-- | The 'Action' of removing a game from the current 'Assignment'.
dismissGameAction :: MonadBB4GGInstance LockISt c m => Action Int m
dismissGameAction =
  do
    i <- askParsed "game identifier"
    () <- join $
      runUnlocked @LockISt $
        do
          assig <- use $ currentAssigIS . valueTS
          if M.member i assig
            then do
              now <- liftIO getCurrentTime
              modifying (currentAssigIS . valueStampedTS now) $ M.delete i
              resetComputation
              return $ return ()
            else
              return $
                throwError
                  ( "This game has already been dismissed from the \
                    \assignment. \
                    \You will be redirected to the main page...\
                    \"
                  , Just ".."
                  )
    broadcastMain
    return (Nothing, "..")

-- | The 'Action' of removing a player from the current 'Assignment'.
dismissPlayerAction :: MonadBB4GGInstance LockISt c m => Action PlayerID m
dismissPlayerAction =
  do
    pID <- askParsed "player ID"
    () <- join $
      runUnlocked @LockISt $
        do
          mPlayer <- preuse $ currentAssigIS . valueTS . traverse . _2 . ix pID
          if isJust mPlayer
            then do
              now <- liftIO getCurrentTime
              modifying
                (currentAssigIS . valueStampedTS now)
                ( M.filter (not . S.null . snd)
                    . set (traverse . _2 . at pID) Nothing
                )
              resetComputation
              return $ return ()
            else
              return $
                throwError
                  ( "This player has already been dismissed from the \
                    \assignment. \
                    \You will be redirected to the main page...\
                    \"
                  , Just ".."
                  )
    broadcastMain
    return (Nothing, "..")

-- | The 'Action' of removing the current 'Assignment'.
dismissAllAction :: MonadBB4GGInstance LockISt c m => Action () m
dismissAllAction =
  do
    () <-
      join $
        runUnlocked @LockISt $
          ifM
            (fmap M.null . use $ currentAssigIS . valueTS)
            ( return $
                throwError
                  ( "The assignment has already been dismissed. \
                    \You will be redirected to the main page...\
                    \"
                  , Just ".."
                  )
            )
            ( do
                now <- liftIO getCurrentTime
                assign (currentAssigIS . valueStampedTS now) M.empty
                resetComputation
                return $ return ()
            )
    broadcastMain
    return (Nothing, "..")

-- | The 'Action' of adding BGG ids (and recommended player numbers) to the games.
updateWithBGGAction :: MonadBB4GGInstance LockIBg c m => Action [BGGThingType] m
updateWithBGGAction =
  do
    types <- S.fromList <$> askParsed "BGG thing types"
    when (S.null types) $ throwError ("No BGG thing types given", Nothing)
    () <- liftForkableMBI $
      void $
        forkIO $
          runUnlockedOrNotVoid @LockIBg False $
            do
              games <- obtaining gamesIS
              gamesUpdate <- updateGamesWithBGG True types games
              runUnlockedVoid @LockISt $
                do
                  modifying gamesIS gamesUpdate
                  saveInstanceStateUnlocked
    return (Just "Linking games with BGG in progress.", "games/modify/")

-- | The 'Action' of importing games from a BGG collection.
importFromBGGCollectionAction :: MonadBB4GGInstance LockIBg c m => Action (BGGThingType, String) m
importFromBGGCollectionAction =
  do
    (thingType, user) <- askParsed "Username or BGG thing type"
    eIDs <- requestBGGCollection thingType user
    ids <-
      either
        (\e -> throwError (showBGGException e, Nothing))
        return
        eIDs
    () <- liftForkableMBI $
      void $
        forkIO $
          runUnlockedVoid @LockIBg $
            do
              addGames <- importBGGIDs ids
              runUnlockedVoid @LockISt $ modifying gamesIS addGames
              saveInstanceStateUnlocked
    return
      ( Just $
          "Importing "
            ++ show (length ids)
            ++ " games from "
            ++ user
            ++ "'s BGG collection."
      , "games/modify/"
      )
