module BB4GG.Web.Page.Instance.Main
  ( instanceMainPage
  , renderMainContent
  ) where

import BB4GG.Core

import BB4GG.Types.Util
  ( assignedPlayers
  , displayPlayerStatus
  , gameScoreInfo
  , readMaybePlayerStatus
  , requiresReady
  , showPlayerStatus
  , waitingPlayers
  )
import BB4GG.Util (sortMap)
import BB4GG.Web.Util.Splices
  ( assignmentSplices
  , gameScoreSplices
  , ifSplices
  , maybeValueSplice
  , normalGameSplices
  , playerDataSplices
  , stringSplice
  )

import Control.Arrow ((&&&))
import Control.Lens (view, (^.))
import Data.List (sortOn)
import Data.Map.Syntax ((##))
import Heist (Splices, getParamNode)
import Heist.Interpreted
  ( Splice
  , callTemplate
  , mapSplices
  , runChildrenWith
  , textSplice
  )
import Snap.Core (MonadSnap)
import Text.XmlHtml (getAttribute)

import Data.ByteString.Lazy qualified as BS
import Data.Map.Strict qualified as M
import Data.Ord qualified as Ord
import Data.Text qualified as T

-- | The main page of the instance displaying the current assignment and the current preferences.
instanceMainPage :: MonadBB4GGInstance' MonadSnap m => m ()
instanceMainPage =
  respondWithSplices "/instance/main/page"
    . instanceMainPageSplices
    =<< obtain

-- | Generates the 'Splices' that are used to render the instance main page.
instanceMainPageSplices :: MonadFail m => InstanceState -> Splices (Splice m)
instanceMainPageSplices state =
  do
    ifSplices . M.fromList $
      [ ("requireReady", requiresReady state)
      , ("gamesInProgress", gamesInProgress)
      ]
    assignmentSplices state (state ^. currentAssigIS . valueTS) Nothing
    playersLinesSplices state
    preferencesTableSplices state
  where
    gamesInProgress = not . M.null $ state ^. currentAssigIS . valueTS

-- | 'Splices' for rendering names of the players in groups according to their statuses.
playersLinesSplices :: MonadFail m => InstanceState -> Splices (Splice m)
playersLinesSplices state =
  "players-line" ##
    do
      mStatusText <- getAttribute "status" <$> getParamNode
      case mStatusText of
        Nothing ->
          fail
            "Encountered a `players-line` tag without a `status` attribute."
        Just statusText ->
          if statusText == "Playing"
            then lineSplice Nothing
            else case readMaybePlayerStatus . T.unpack $ statusText of
              Nothing -> fail "Can't parse player status."
              Just ps' -> lineSplice $ Just ps'
  where
    lineSplice mStatus =
      callTemplate "/instance/main/players-line" $
        do
          "players-line-title" ##
            textSplice $
              maybe "Playing" displayPlayerStatus mStatus
          "players"
            ## mapSplices id
              . sortMap (playerSpliceWith mStatus)
              . M.filterWithKey (predicateFor mStatus)
              . M.map (view valueTS)
            $ state ^. playersIS
    playerSpliceWith mStatus (pID, pd) =
      runChildrenWith $
        do
          playerDataSplices pID pd
          "player-arrows" ##
            maybeValueSplice (mapSplices arrowSplice . arrows) mStatus
    arrows ReadyPS = [("double-down", InactivePS)]
    arrows WantsToPlayPS =
      [("up", ReadyPS) | requiresReady state] ++ [("down", InactivePS)]
    arrows InactivePS = [("up", WantsToPlayPS)]
    arrowSplice (direction, targetStatus) =
      runChildrenWith $
        do
          "arrow-target-status" ## stringSplice $ showPlayerStatus targetStatus
          "arrow-direction" ## textSplice direction
    predicateFor Nothing pID _ =
      M.member pID $ assignedPlayers state
    predicateFor (Just status) pID pd =
      pd ^. statusPD == status && (not . M.member pID $ assignedPlayers state)

-- | 'Splices' for rendering a summary of players' preferences if there are "waiting" players.
preferencesTableSplices :: Monad m => InstanceState -> Splices (Splice m)
preferencesTableSplices state =
  "preferences" ##
    if M.null $ waitingPlayers state
      then return []
      else
        runChildrenWith $
          "game-row" ##
            mapSplices gamePrefSplice preferredGames
  where
    gamePrefSplice ((ngID, gd), _) =
      runChildrenWith $
        do
          normalGameSplices ngID gd
          gameScoreSplices state (NormalGID ngID)
    preferredGames =
      sortOn popularity
        . filter (\(_, (prefPlayers, _, _)) -> not $ null prefPlayers)
        . map (id &&& (gameScoreInfo state . NormalGID . fst))
        . M.toList
        $ state ^. gamesIS
    popularity ((_, gd), (prefPlayers, scoreSum, averageScore)) =
      Ord.Down
        (gd ^. isAvailableGD, scoreSum, averageScore, length prefPlayers, gd)

-- | Render the content of the instance main page.
renderMainContent :: MonadBB4GGInstance' c m => m BS.ByteString
renderMainContent =
  (maybe "Couldn't render content for websockets." fst <$>)
    . renderWithSplices "/instance/main/content"
    . instanceMainPageSplices
    =<< obtain
