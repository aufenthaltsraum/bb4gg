module BB4GG.Web.Page.Instance.Configure (configureInstancePage) where

import BB4GG.Core

import BB4GG.Core.Computation.Handling (resetComputation)
import BB4GG.Util (toRoundSeconds)
import BB4GG.Web.Util.Form (integralForm, optionalIntegralForm)
import BB4GG.Web.Util.Splices (digestiveSplices')
import BB4GG.Web.Websockets.Broadcast (broadcastMain)

import Control.Lens (assign, modifying, use, view, (^.))
import Control.Monad (join, when)
import Data.Maybe (fromMaybe, isJust, isNothing)
import Snap.Core (MonadSnap, redirect)
import Text.Digestive
  ( Form
  , bool
  , check
  , (.:)
  )
import Text.Digestive.Snap (runForm)

-- | The page where you can change the configuration of the current instance.
configureInstancePage :: MonadBB4GGInstance LockISt MonadSnap m => m ()
configureInstancePage =
  join $
    runUnlocked @LockISt $
      do
        config <- use configurationIS
        result <-
          runForm "configureInstanceForm" . configureInstanceForm $ config
        case result of
          (digestiveView, Nothing) ->
            return
              . respondWithSplices "/instance/configure"
              . digestiveSplices'
              $ digestiveView
          (_, Just newConfig) ->
            do
              assign configurationIS newConfig
              let requireReadyDisabled =
                    isJust (config ^. requireReadyIC)
                      && isNothing (newConfig ^. requireReadyIC)
                  requireReadyEnabled =
                    isNothing (config ^. requireReadyIC)
                      && isJust (newConfig ^. requireReadyIC)
                  assignmentsToComputeChanged =
                    config ^. assignmentsToComputeIC
                      /= newConfig ^. assignmentsToComputeIC
              when requireReadyDisabled $
                modifying
                  (playersIS . traverse . valueUnstampedTS . statusPD)
                  (\case ReadyPS -> WantsToPlayPS; s -> s)
              when
                ( requireReadyDisabled
                    || requireReadyEnabled
                    || assignmentsToComputeChanged
                )
                resetComputation
              return $
                do
                  broadcastMain
                  saveInstanceStateUnlocked
                  redirect "."

-- | Form for changing the instance configuration.
configureInstanceForm
  :: Monad m
  => InstanceConfiguration
  -- ^ For prefilling with the current configuration.
  -> Form String m InstanceConfiguration
configureInstanceForm ic =
  do
    listed <- "listed" .: bool (Just $ ic ^. listedIC)
    toCompute <-
      "assignments-to-compute"
        .: ( check "Assignments to compute is negative" (>= 0)
              . integralForm "Assignments to compute"
              . Just
              . view assignmentsToComputeIC
           )
          ic
    requireReady <-
      "require-ready-bool" .: bool (Just . isJust . view requireReadyIC $ ic)
    duration <-
      "require-ready-duration"
        .: ( fmap fromInteger
              . check "Duration is negative" (>= 0)
              . fmap (fromMaybe 0)
              . optionalIntegralForm "Duration"
              . fmap toRoundSeconds
              . view requireReadyIC
           )
          ic
    mAutoDeactivationHours <-
      "auto-deactivation-hours"
        .: ( check "Duration is not positive" (all (> 0))
              . optionalIntegralForm "Duration"
              . view autoDeactivationHoursIC
           )
          ic
    pure $
      InstanceConfiguration
        listed
        toCompute
        (if requireReady then Just duration else Nothing)
        mAutoDeactivationHours
