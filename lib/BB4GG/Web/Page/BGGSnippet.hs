module BB4GG.Web.Page.BGGSnippet (bggSnippet) where

import BB4GG.Core

import BB4GG.Web.Util.Splices
  ( eitherSplice
  , ifSplices
  , maybeValueSplice
  , rangeSplice
  , showSplice
  , stringSplice
  )

import Control.Lens (view, (^.))
import Control.Monad (mfilter)
import Data.Colour (Colour, blend)
import Data.Colour.Names
  ( crimson
  , dimgrey
  , dodgerblue
  , orangered
  , seagreen
  , yellowgreen
  )
import Data.Colour.SRGB (sRGB24show)
import Data.Either.Combinators (mapLeft, whenLeft)
import Data.Function (on)
import Data.List.Extra (splitOn)
import Data.Map.Syntax ((##))
import Data.Maybe (isJust)
import Heist (Splices, getParamNode)
import Heist.Interpreted (Splice, mapSplices, runChildrenWith, textSplice)
import Snap.Core
  ( MonadSnap
  , addHeader
  , getQueryParam
  , modifyResponse
  , setResponseCode
  , writeBS
  )
import Text.Printf (printf)
import Text.Read (readMaybe)
import Text.XmlHtml (getAttribute)

import Data.ByteString.UTF8 qualified as BS

import Data.Map qualified as M
import Data.Set qualified as S

-- | If only an "id" is passed, renders a BGG tooltip; if only a "query" is passed, renders BGG search results.
-- Otherwise returns a 400 status.
bggSnippet :: MonadBB4GG MonadSnap m => m ()
bggSnippet =
  do
    mGameID <- (readMaybe . BS.toString =<<) <$> getQueryParam "id"
    mQuery <- fmap BS.toString <$> getQueryParam "query"
    case (mGameID, mQuery) of
      (Just gID, Nothing) -> bggTooltip gID
      (Nothing, Just query) -> bggSearch query
      _ -> modifyResponse $ setResponseCode 400

-- | Renders BGG search results for the query.
bggSearch :: MonadBB4GG MonadSnap m => String -> m ()
bggSearch query =
  do
    dump <- isJust <$> getQueryParam "dump"
    mMaxNum <- (readMaybe . BS.toString =<<) <$> getQueryParam "max"
    mTypes <-
      (mapM readMaybeBGGThingType . splitOn "," . BS.toString =<<)
        <$> getQueryParam "type"
    case mTypes of
      Nothing -> modifyResponse $ setResponseCode 400
      Just types ->
        do
          results <-
            runUnlocked' $
              fmap (maybe id take mMaxNum)
                <$> requestBGGSearch False (S.fromList types) query
          whenLeft results $
            modifyResponse . setResponseCode . bggExceptionResponseCode
          if dump
            then do
              modifyResponse
                (addHeader "Content-Type" "text/plain; charset=UTF-8")
              writeBS . BS.fromString $ show results
            else
              respondWithSplices "bgg-search" . bggSearchSplices query $
                mapLeft showBGGException results

-- | The splices to access multiple 'BGGSearchResult's.
bggSearchSplices
  :: MonadFail m
  => String
  -> Either String [BGGSearchResult]
  -> Splices (Splice m)
bggSearchSplices query eErrorResults =
  "any-errors" ##
    eitherSplice
      (("error-message" ##) . stringSplice)
      resultsSplices
      eErrorResults
  where
    resultsSplices results =
      do
        "bgg-search-query" ## stringSplice query
        ifSplices $ M.fromList [("noResults", null results)]
        "bgg-search-results" ##
          mapSplices (runChildrenWith . bggSearchResultSplices) results

-- | The splices to access a 'BGGSearchResult'.
bggSearchResultSplices :: Monad m => BGGSearchResult -> Splices (Splice m)
bggSearchResultSplices result =
  do
    "bgg-search-result-id" ## showSplice $ result ^. idBGGSR
    "bgg-search-result-type" ## stringSplice . showType $ result ^. typeBGGSR
    "bgg-search-result-name" ## textSplice $ result ^. nameBGGSR
    "bgg-search-result-year" ##
      maybeValueSplice showSplice $
        result ^. yearBGGSR

-- | Returns an HTML snippet containing information about the game belonging to the given BGG id.
bggTooltip :: MonadBB4GG MonadSnap m => BGGID -> m ()
bggTooltip gID =
  do
    dump <- isJust <$> getQueryParam "dump"
    emGame <- runUnlocked' $ retrieveBGGThing gID
    eErrorGame <-
      case emGame of
        Left err ->
          do
            modifyResponse . setResponseCode $ bggExceptionResponseCode err
            return . Left $ showBGGException err
        Right Nothing ->
          do
            modifyResponse $ setResponseCode 404
            return $ Left "BGG identifier does not exist"
        Right (Just game) -> return $ Right game
    if dump
      then do
        modifyResponse
          (addHeader "Content-Type" "text/plain; charset=UTF-8")
        writeBS . BS.fromString $ show emGame
      else respondWithSplices "bgg-tooltip" $ bggTooltipSplices eErrorGame

-- | The HTTP response code associated to a 'BGGException'.
bggExceptionResponseCode :: BGGException -> Int
bggExceptionResponseCode (BGGXMLException _) = 500
bggExceptionResponseCode (BGGParseException _) = 500
bggExceptionResponseCode (BGGHttpException _) = 503

-- | The Splices for the BGG tooltip.
bggTooltipSplices :: Monad m => Either String BGGThing -> Splices (Splice m)
bggTooltipSplices eErrorGame =
  "any-errors" ##
    eitherSplice (("error-message" ##) . stringSplice) bggGameSplices eErrorGame

-- | The Splices to access information of the BGG game.
bggGameSplices :: Monad m => BGGThing -> Splices (Splice m)
bggGameSplices game =
  do
    "bgg-game-name" ## textSplice $ game ^. nameBGGT
    "bgg-game-type" ## stringSplice . showType $ game ^. typeBGGT
    "bgg-game-id" ## showSplice $ game ^. idBGGT
    "bgg-game-rank" ## maybeValueSplice showSplice $ game ^. rankBGGT
    let rating = game ^. ratingBGGT
    "bgg-game-rating" ## maybeValueSplice (stringSplice . printf "%.1f") rating
    "bgg-game-rating-color" ##
      stringSplice . sRGB24show $
        maybe
          dimgrey
          ( interpolate $
              M.fromList [(5, crimson), (7, dodgerblue), (8, seagreen)]
          )
          rating
    let weight = game ^. weightBGGT
    "bgg-game-weight" ## maybeValueSplice (stringSplice . printf "%.1f") weight
    "bgg-game-weight-color" ##
      stringSplice . sRGB24show $
        maybe
          dimgrey
          ( interpolate . M.fromList $
              [(1, seagreen), (2, yellowgreen), (4, orangered), (5, crimson)]
          )
          weight
    "bgg-game-length" ##
      (rangeSplice `on` maybe "?" show)
        (game ^. minPlayTimeBGGT)
        (game ^. maxPlayTimeBGGT)
    "bgg-game-players" ##
      (rangeSplice `on` maybe "?" show)
        (game ^. minPlayersBGGT)
        (game ^. maxPlayersBGGT)
    "bgg-game-players-recommended" ##
      maybeValueSplice
        (mapSplices (uncurry (rangeSplice `on` show)) . setToRanges)
        (mfilter (not . S.null) . Just $ game ^. recommendedPlayersBGGT)
    "bgg-game-thumbnail" ##
      maybeValueSplice textSplice $
        game ^. thumbnailURLBGGT
    "bgg-link-types" ##
      mapSplices (bggLinkTypeSplice (game ^. linksBGGT)) $
        linkTypesToShow game
    "bgg-links" ##
      do
        mType <- getAttribute "type" <$> getParamNode
        mapSplices (runChildrenWith . bggLinkSplices)
          . maybe id (\t -> filter $ (== t) . view typeBGGL) mType
          $ game ^. linksBGGT
    "bgg-game-year" ## maybeValueSplice showSplice $ game ^. yearBGGT

-- | Pretty print a 'BGGThingType'.
showType :: BGGThingType -> String
showType BoardgameBGGTT = "Board game"
showType BoardgameexpansionBGGTT = "Board game expansion"
showType BoardgameaccessoryBGGTT = "Board game accessory"
showType VideogameBGGTT = "Video game"
showType RpgitemBGGTT = "RPG item"
showType RpgissueBGGTT = "RPG issue"

-- | The link types to show, as well as a label for that type.
linkTypesToShow :: BGGThing -> [(BGGLinkType, String)]
linkTypesToShow thing
  | thing ^. typeBGGT `elem` [BoardgameBGGTT, BoardgameexpansionBGGTT] =
      [("boardgamecategory", "Categories"), ("boardgamemechanic", "Mechanics")]
  | thing ^. typeBGGT == VideogameBGGTT =
      [ ("videogamegenre", "Genres")
      , ("videogametheme", "Themes")
      , ("videogameplatform", "Platforms")
      ]
  | thing ^. typeBGGT == RpgitemBGGTT =
      [("rpggenre", "Genres"), ("rpgmechanic", "Mechanics")]
  | otherwise = []

-- | Renders those Links that match the given link type; the string is used as a label.
bggLinkTypeSplice :: Monad m => [BGGLink] -> (BGGLinkType, String) -> Splice m
bggLinkTypeSplice links (linkType, linkTypeLabel) =
  runChildrenWith $
    do
      "bgg-link-type-label" ## stringSplice linkTypeLabel
      "bgg-links" ##
        mapSplices (runChildrenWith . bggLinkSplices) $
          filter ((== linkType) . view typeBGGL) links

-- | The Splices to access information of the BGG link.
bggLinkSplices :: Monad m => BGGLink -> Splices (Splice m)
bggLinkSplices link =
  do
    "bgg-link-name" ## textSplice $ link ^. nameBGGL
    "bgg-link-id" ## showSplice $ link ^. idBGGL

-- | Extracts the consecutive ranges from a Set.
setToRanges :: Integral n => S.Set n -> [(n, n)]
setToRanges s =
  case S.toAscList s of
    [] -> []
    l@(start : _) -> setToRanges' start l
  where
    setToRanges' _ [] = []
    setToRanges' start [curr] = [(start, curr)]
    setToRanges' start (curr : l@(next : _))
      | next == curr + 1 = setToRanges' start l
      | otherwise = (start, curr) : setToRanges' next l

-- | Interpolates between some colors.
interpolate :: (Fractional a, Ord a) => M.Map a (Colour a) -> a -> Colour a
interpolate points x =
  case (mLower, mUpper) of
    (Just (l, lc), Just (u, uc))
      | l == u -> lc
      | otherwise -> blend ((x - l) / (u - l)) uc lc
    (Just (_, lc), Nothing) -> lc
    (Nothing, Just (_, uc)) -> uc
    (Nothing, Nothing) -> error "Empty map given"
  where
    mLower = M.lookupLE x points
    mUpper = M.lookupGE x points
