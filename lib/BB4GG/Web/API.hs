module BB4GG.Web.API (dumpInstanceState) where

import BB4GG.Core

import Data.Aeson (encode)
import Snap.Core
  ( MonadSnap (..)
  , addHeader
  , modifyResponse
  , writeLBS
  )

-- | Dumps the instance state as JSON.
dumpInstanceState :: MonadBB4GGInstance' MonadSnap m => m ()
dumpInstanceState =
  do
    modifyResponse (addHeader "Content-Type" "application/json; charset=UTF-8")
    writeLBS . encode . toSavableInstanceState =<< obtain
