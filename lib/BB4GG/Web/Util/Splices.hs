{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module BB4GG.Web.Util.Splices
  ( MsgType (..)
  , Path
  , assignmentSplices
  , eitherSplice
  , errorPageSplices
  , digestiveSplices'
  , gameInfoSplice
  , gameInfoSplice'
  , gameScoreSplices
  , ifAttributeSplices
  , ifSplices
  , lockedTimeSplices
  , maybeValueSplice
  , messagePageSplices
  , normalGameSplices
  , playerDataSplices
  , rangeSplice
  , runChildrenWith'
  , showSplice
  , stringSplice
  ) where

import BB4GG.Core

import BB4GG.Types.Util (gamePreferenceScores, gameScoreInfo, showPlayerStatus)
import BB4GG.Util (rangeToString, sortMap, toRoundSeconds)

import Control.Lens (ix, view, (^.), (^?), (^?!))
import Control.Monad (mfilter)
import Control.Monad.IO.Class (MonadIO)
import Data.Either.Combinators (maybeToLeft)
import Data.Function (on)
import Data.List (intercalate, nub, sortOn)
import Data.Map.Syntax ((##))
import Data.Maybe (isJust)
import Data.Text.Encoding (decodeUtf8)
import Data.Time (UTCTime)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import Heist (AttrSplice, Splices, getParamNode, localHS)
import Heist.Interpreted
  ( Splice
  , bindAttributeSplices
  , bindSplices
  , callTemplate
  , callTemplateWithText
  , mapSplices
  , runChildren
  , runChildrenWith
  , runNodeList
  , textSplice
  )
import Heist.Splices (ifElseISplice)
import Text.Digestive (View, childErrors, errors)
import Text.Digestive.Heist (digestiveSplices)
import Text.XmlHtml (childNodes, getAttribute, tagName)

import Data.ByteString qualified as BS
import Data.Map qualified as M
import Data.Set qualified as S
import Data.Text qualified as T

-- | For paths to URLs.
type Path = BS.ByteString

-- | Like 'textSplice', but uses a string instead.
stringSplice :: Monad m => String -> Splice m
stringSplice = textSplice . T.pack

-- | Passes the result of 'show' to 'stringSplice'.
showSplice :: (Monad m, Show a) => a -> Splice m
showSplice = stringSplice . show

-- | 'Splices' for error messages.
-- The tag to use is <errors ref="someRef" />.
errorsSplices :: MonadFail m => View T.Text -> Splices (Splice m)
errorsSplices v =
  do
    "errors" ## errorsSpliceWith errors
    "child-errors" ## errorsSpliceWith childErrors
  where
    errorsSpliceWith extractor =
      do
        mRef <- getAttribute "ref" <$> getParamNode
        case mRef of
          Just ref ->
            case extractor ref v of
              [] ->
                return []
              es ->
                messageSplice Error
                  . unwords
                  . map ((<> ".") . T.unpack)
                  $ nub es
          Nothing ->
            fail "Encountered an error tag without a `ref` attribute."

-- | Combination of the standard digestive 'Splices' and the 'Splices' for errors.
digestiveSplices'
  :: (MonadFail m, MonadIO m) => View String -> Splices (Splice m)
digestiveSplices' v =
  let tView = T.pack <$> v in digestiveSplices tView <> errorsSplices tView

data MsgType = Info | Error deriving (Show)

-- | 'Splices' for a <span> showing a message.
messageSplices
  :: Monad m
  => MsgType
  -- ^ The type of the message.
  -> String
  -- ^ The message to show.
  -> Splices (Splice m)
messageSplices msgType msg =
  do
    "message" ## stringSplice msg
    "message-type" ## stringSplice $ case msgType of Info -> "info"; Error -> "error"

-- | 'Splices' for a message page.
messagePageSplices
  :: MonadFail m
  => MsgType
  -- ^ The type of the message.
  -> String
  -- ^ The message to show.
  -> Maybe Path
  -- ^ Where to redirect.
  -> Splices (Splice m)
messagePageSplices msgType msg mRedirect =
  do
    "page-name" ## stringSplice (show msgType)
    ifSplices . M.singleton "redirect" $ isJust mRedirect
    foldMap (("redirect-location" ##) . textSplice . decodeUtf8) mRedirect
    messageSplices msgType msg

-- | A 'Splice' showing an error message.
messageSplice :: Monad m => MsgType -> String -> Splice m
messageSplice msgType msg = callTemplate "/message-span" $ messageSplices msgType msg

-- | 'Splices' for an error page.
errorPageSplices :: MonadFail m => String -> Maybe Path -> Splices (Splice m)
errorPageSplices = messagePageSplices Error

-- | An @if@ tag implementation.
--
-- Every @if@ tag should contain a @var@ attribute, which is looked up in the 'Map' that is passed to this function, where missing values are assumed to be @False@.
-- The contents of the @if@ tag are then split at an (optional) @else@ tag and the first or second part is rendered depending on whether the boolean the @var@ attribute is referring to is @True@ or @False@.
--
-- If an @if@ tag does not contain a @var@ attribute, the associated 'Splice' @fail@s with an appropriate error message.
ifSplices :: MonadFail m => M.Map T.Text Bool -> Splices (Splice m)
ifSplices env =
  "if" ##
    do
      mVarName <- getAttribute "var" <$> getParamNode
      case mVarName of
        Nothing -> fail "Encountered an `if` tag without a `var` attribute."
        Just varName -> ifElseISplice $ Just True == M.lookup varName env

-- | 'Splices' containing all the information given (plus the prerendered player range).
--
-- Not all callers will need all the information included, but the overhead introduced by it should be negligible.
normalGameSplices :: Monad m => NormalGameID -> GameData -> Splices (Splice m)
normalGameSplices gID gd =
  do
    "game-id" ## showSplice $ gID
    "game-name" ## stringSplice $ gd ^. nameGD
    "game-comment" ##
      maybeValueSplice stringSplice . mfilter (not . null) . Just $
        gd ^. commentGD
    "game-bgg-id" ## maybeValueSplice showSplice $ gd ^. bggIDGD
    let minPlayers = gd ^. minPlayersGD
        maxPlayers = gd ^. maxPlayersGD
        recommendedMinPlayers = gd ^. recommendedMinPlayersGD
        recommendedMaxPlayers = gd ^. recommendedMaxPlayersGD
    "game-min" ## showSplice minPlayers
    "game-max" ## showSplice maxPlayers
    "game-player-range" ## (rangeSplice `on` show) minPlayers maxPlayers
    "game-recommended-min" ## showSplice recommendedMinPlayers
    "game-recommended-max" ## showSplice recommendedMaxPlayers
    "game-recommended-player-range" ##
      (rangeSplice `on` show) recommendedMinPlayers recommendedMaxPlayers
    "game-copies" ## showSplice $ gd ^. maxCopiesGD
    "game-availability" ## showSplice $ gd ^. isAvailableGD

-- | 'Splices' containing information about the player preferences for a game.
gameScoreSplices
  :: Monad m => InstanceState -> GameID -> Splices (Splice m)
gameScoreSplices state gID =
  do
    "score-sum" ## showSplice scoreSum
    "average-score" ## showSplice averageScore
    "average-score-round" ## showSplice (round averageScore :: Int)
    "preferring-players" ##
      mapSplices id . sortMap playerSplice $
        M.map
          (view valueTS)
          players
  where
    (players, scoreSum, averageScore) = gameScoreInfo state gID
    playerSplice (pID, pd) =
      runChildrenWith $
        do
          playerDataSplices pID pd
          "player-preferences" ##
            stringSplice
              ( intercalate ", "
                  . map
                    ( \(range, score) ->
                        rangeToString range ++ ": " ++ show score
                    )
                  . M.foldrWithKey' takeConstRanges []
                  . gamePreferenceScores
                  $ pd ^?! prefPD . ix gID
              )
    takeConstRanges num score ranges =
      case ranges of
        (((a, b), score') : rs)
          | a == num + 1 && score == score' ->
              ((num, b), score) : rs
        _ -> ((num, num), score) : ranges

-- | If parameters are equal, provides a single value, otherwise both.
rangeSplice :: Monad m => String -> String -> Splice m
rangeSplice from to =
  runChildrenWith . ("range" ##) $
    eitherSplice
      (\single -> "single-value" ## stringSplice single)
      ( \(from', to') ->
          do
            "from-value" ## stringSplice from'
            "to-value" ## stringSplice to'
      )
      ( if from == to
          then Left from
          else Right (from, to)
      )

-- | Create a 'Splice' containing the name of the game and its player number tresholds.
--
-- The resulting 'Splice' calls the template @/instance/game-info/normal@ or @/instance/game-info/special@ according to the game type.
gameInfoSplice' :: Monad m => GameID -> GameData -> Splice m
gameInfoSplice' (SpecialGID NotPlayingSG) gd =
  callTemplateWithText "/instance/game-info/special"
    . ("game-name" ##)
    . T.pack
    $ gd ^. nameGD
gameInfoSplice' (NormalGID gID) gd =
  callTemplate "/instance/game-info/normal" $ normalGameSplices gID gd

-- | Create a 'Splice' containing the name of the game and its player number tresholds.
gameInfoSplice :: Monad m => Games -> GameID -> Splice m
gameInfoSplice gs gID =
  case gs ^? gameDataG gID of
    Nothing -> messageSplice Error "Error: game not found."
    Just gd -> gameInfoSplice' gID gd

-- | The `Splices` for accessing player data.
playerDataSplices :: Monad m => PlayerID -> PlayerData -> Splices (Splice m)
playerDataSplices pID pd =
  do
    "player-name" ## stringSplice $ pd ^. namePD
    "player-id" ## showSplice pID
    "player-status" ## stringSplice . showPlayerStatus $ pd ^. statusPD

-- | The `Splices` for accessing an assignment.
assignmentSplices
  :: Monad m
  => InstanceState
  -> Assignment
  -> Maybe AssignmentScore
  -> Splices (Splice m)
assignmentSplices state assig mAS =
  "assignment-games"
    ## mapSplices assignmentGameSplice
      . sortOn (\(_, (gID, _)) -> state ^? gamesIS . gameDataG gID)
    $ M.toList assig
  where
    assignmentGameSplice (i, (gID, pIDs)) =
      runChildrenWith $
        do
          "game-info" ## gameInfoSplice (state ^. gamesIS) gID
          "game-playing-players"
            ## mapSplices id
              . sortMap
                ( \(pID, pd) -> runChildrenWith $
                    do
                      mapM_ (("player-score" ##) . showSplice) $
                        M.lookup pID . _playerScoresAS =<< mAS
                      playerDataSplices pID pd
                )
              . M.map (view valueTS)
              . M.filterWithKey (\pID _ -> S.member pID pIDs)
            $ state ^. playersIS
          "assignment-game-id" ## showSplice i

-- | 'Splices' that map @locked-time@ to number of seconds in the given `NominalDiffTime`.
-- Results in empty 'Splices' if the argument is @Nothing@.
lockedTimeSplices :: Monad m => Maybe UTCTime -> Splices (Splice m)
lockedTimeSplices =
  foldMap
    (("locked-time" ##) . showSplice . toRoundSeconds . utcTimeToPOSIXSeconds)

-- | A splice that returns its children up to an optional <else />-tag when the passed Maybe is Just.
-- In that case <value /> will be the result of the splice obtained by evaluating the function on the Just value.
-- When the passed Maybe is Nothing, returns everything after the <else />.
maybeValueSplice :: Monad m => (a -> Splice m) -> Maybe a -> Splice m
maybeValueSplice f =
  eitherSplice (("value" ##) . f) (const mempty) . maybeToLeft ()

-- | A splice that returns its children up to an <else />-tag only when passed 'Either' is 'Left' and everything afterwards when it is 'Right'.
-- In both cases the content is evaluated with the given 'Splices'.
eitherSplice
  :: Monad m
  => (a -> Splices (Splice m))
  -> (b -> Splices (Splice m))
  -> Either a b
  -> Splice m
eitherSplice f g e = getParamNode >>= (rewrite . childNodes)
  where
    rewrite nodes =
      case e of
        Left a -> runNodeListWith (f a) beforeElse
        Right b -> runNodeListWith (g b) $ drop 1 fromElse
      where
        (beforeElse, fromElse) = break (\n -> tagName n == Just "else") nodes
    runNodeListWith s nodes = localHS (bindSplices s) $ runNodeList nodes

-- An attribute splice called "if-then", that when given a value of the form "var=>attr", looks up var in the given 'Map' and sets the attribute attr if and only if the result is 'True'.
ifAttributeSplices :: Monad m => M.Map T.Text Bool -> Splices (AttrSplice m)
ifAttributeSplices vars =
  "if-then" ##
    \val ->
      maybe (error "Erroneous if-then attribute encountered.") return $
        do
          let (var, attr') = T.breakOn thenSymbol val
          attr <- T.stripPrefix thenSymbol attr'
          bool <- vars M.!? var
          return [(attr, "") | bool]
  where
    thenSymbol = "=>"

-- | Like 'runChildrenWith', but also binds attribute splices.
runChildrenWith'
  :: Monad m => Splices (Splice m) -> Splices (AttrSplice m) -> Splice m
runChildrenWith' s as =
  localHS (bindSplices s . bindAttributeSplices as) runChildren
