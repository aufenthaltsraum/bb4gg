{-# OPTIONS_GHC -Wno-orphans #-}

module BB4GG.Config.OrphanFromConfig where

import Conferer.FromConfig (FromConfig (..), fetchFromConfigByRead)
import Data.Time.Clock (NominalDiffTime)

-- We need this instance because the default implementation using generics can't parse values like "0.1s".
instance FromConfig NominalDiffTime where
  fetchFromConfig = fetchFromConfigByRead
