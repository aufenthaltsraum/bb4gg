module BB4GG.Tests.GameData (games) where

import BB4GG.Types

import Data.Map qualified as M

games :: Games
games =
  M.fromList . zip [0 ..] $
    [ -- https://en.wikipedia.org/wiki/Hanabi_(card_game)
      GameData
        { _nameGD = "Hanabi"
        , _commentGD = ""
        , _bggIDGD = Just 98778
        , _minPlayersGD = 2
        , _maxPlayersGD = 5
        , _recommendedMinPlayersGD = 3
        , _recommendedMaxPlayersGD = 5
        , _maxCopiesGD = 1
        , _isAvailableGD = True
        }
    , -- https://en.wikipedia.org/wiki/Race_for_the_Galaxy
      GameData
        { _nameGD = "Race for the Galaxy"
        , _commentGD = ""
        , _bggIDGD = Just 28143
        , _minPlayersGD = 2
        , _maxPlayersGD = 4
        , _recommendedMinPlayersGD = 2
        , _recommendedMaxPlayersGD = 4
        , _maxCopiesGD = 1
        , _isAvailableGD = True
        }
    , -- https://en.wikipedia.org/wiki/Pandemic_(board_game)
      GameData
        { _nameGD = "Pandemic"
        , _commentGD = ""
        , _bggIDGD = Just 30549
        , _minPlayersGD = 1
        , _maxPlayersGD = 4
        , _recommendedMinPlayersGD = 2
        , _recommendedMaxPlayersGD = 4
        , _maxCopiesGD = 1
        , _isAvailableGD = True
        }
    , -- https://en.wikipedia.org/wiki/Go_(game)
      GameData
        { _nameGD = "Go"
        , _commentGD = ""
        , _bggIDGD = Just 188
        , _minPlayersGD = 2
        , _maxPlayersGD = 2
        , _recommendedMinPlayersGD = 2
        , _recommendedMaxPlayersGD = 2
        , _maxCopiesGD = 1
        , _isAvailableGD = True
        }
    , -- http://en.wikipedia.org/wiki/Charades
      GameData
        { _nameGD = "Pantomime"
        , _commentGD = ""
        , _bggIDGD = Just 5122
        , _minPlayersGD = 4
        , _maxPlayersGD = 10
        , _recommendedMinPlayersGD = 4
        , _recommendedMaxPlayersGD = 10
        , _maxCopiesGD = 100
        , _isAvailableGD = True
        }
    ]
