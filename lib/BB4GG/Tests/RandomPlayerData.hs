module BB4GG.Tests.RandomPlayerData (randomPlayers) where

import BB4GG.Core

import Control.Lens ((^.))
import Control.Monad (replicateM)
import Control.Monad.Random.Lazy
  ( Rand
  , RandomGen
  , evalRand
  , getRandom
  , getRandomR
  , newStdGen
  )
import Data.Time (getCurrentTime)

import Data.Map qualified as M

-- | Returns a given number of players playing the given 'Games'.
-- Every player has a 50% chance to play a given game, and if they do their preference is equally distributed between the minimum and maximum possible preference for a game.
randomPlayers :: Int -> Games -> IO Players
randomPlayers num games =
  do
    now <- getCurrentTime
    gen <- newStdGen
    return
      . M.fromList
      . zipWith
        ( \i pref ->
            ( i
            , stamp now $
                PlayerData
                  { _namePD = "Player" ++ show i
                  , _statusPD = WantsToPlayPS
                  , _prefPD = pref
                  }
            )
        )
        [1 ..]
      . flip evalRand gen
      . replicateM num
      $ randomPreference games

-- | Returns a random 'Preference'.
-- Every game has a 50% chance to be played, and if it is the preference is equally distributed between the minimum and maximum possible preference.
randomPreference :: RandomGen g => Games -> Rand g Preference
randomPreference games =
  M.insert (SpecialGID NotPlayingSG) notPlayingGP
    . M.mapKeys NormalGID
    . M.mapMaybe id
    <$> traverse
      ( \gData ->
          do
            plays <- getRandom
            score <- getRandomR (1, maxPreference)
            if plays
              then
                return . Just $
                  GamePreference
                    { _scoreGP = score
                    , _minPlayersGP = gData ^. minPlayersGD
                    , _maxPlayersGP = gData ^. maxPlayersGD
                    , _extraScoresGP = M.empty
                    , _isActiveGP = True
                    }
              else return Nothing
      )
      games
  where
    notPlayingGD = specialGameData NotPlayingSG
    notPlayingGP =
      GamePreference
        { _scoreGP = 1
        , _minPlayersGP = notPlayingGD ^. minPlayersGD
        , _maxPlayersGP = notPlayingGD ^. maxPlayersGD
        , _extraScoresGP = M.empty
        , _isActiveGP = True
        }
