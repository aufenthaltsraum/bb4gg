{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Core.Monad.Trans.BB4GGInstance
  ( module BB4GG.Core.Monad.BB4GGInstance
  , BB4GGInstanceT
  , initInstance
  , runBB4GGInstanceT
  ) where

import BB4GG.Core.Monad.BB4GGInstance
import BB4GG.Types

import BB4GG.BGG.Request (BGGT)
import BB4GG.Core.Constants (instanceDataPath)
import BB4GG.Util.Monad.Trans.HeistState
  ( BB4GGHeistM
  , HeistStateT (..)
  , MonadHeistState
  )
import BB4GG.Util.Monad.Unlockable (MonadUnlockableLT, runUnlockedOrNotWith)
import BB4GG.Web.Websockets.Server (initWebsocketsServer)

import Control.Applicative (Alternative)
import Control.Concurrent.Forkable (ForkableMonad)
import Control.Concurrent.STM
  ( atomically
  , newEmptyTMVarIO
  , newTVarIO
  , readTVarIO
  , stateTVar
  , writeTVar
  )
import Control.Lens (view, _2)
import Control.Monad (MonadPlus, join)
import Control.Monad.Base (MonadBase (..))
import Control.Monad.Morph (MFunctor, hoist)
import Control.Monad.Reader (MonadReader (..), ReaderT (..), runReaderT)
import Control.Monad.State (MonadState (..))
import Control.Monad.Trans (MonadIO, MonadTrans, lift, liftIO)
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Kind (Constraint, Type)
import Data.List.Extra (enumerate)
import Data.SafeCopy (safePut)
import Data.Serialize (runPut)
import Heist (HeistState)
import Snap.Core (MonadSnap (..))

import Data.Array qualified as A
import Data.ByteString qualified as BS

-- | Type synonym for the reader type of 'BB4GGInstanceT' to make changing it easier.
type ReadBIT = (InstanceName, Instance)

-- | A monad transformer that adds 'MonadBB4GGInstance' functionality to any monad that instantiates 'MonadIO'.
newtype BB4GGInstanceT (c :: (Type -> Type) -> Constraint) m a = BB4GGInstanceT
  { readerBIT :: HeistStateT (ReaderT ReadBIT m) a
  }
  deriving
    ( Alternative
    , Applicative
    , ForkableMonad
    , Functor
    , Monad
    , MonadBase b
    , MonadBaseControl b
    , MonadHeistState
    , MonadIO
    , MonadPlus
    , MonadSnap
    )

-- | Run a 'BB4GGInstanceT' provided the name of the instance and its data.
runBB4GGInstanceT
  :: BB4GGInstanceT c m a
  -> HeistState BB4GGHeistM
  -> ReadBIT
  -> m a
runBB4GGInstanceT (BB4GGInstanceT (HeistStateT b)) = runReaderT . runReaderT b

instance
  ( MonadBaseControl IO m
  , MonadBGG m
  , c (BB4GGInstanceT c m)
  , LiftsThroughEnough c
  , MonadUnlockableLT l (MonadBB4GGInstance' c) (BB4GGInstanceT c m)
  , IsLockI l
  )
  => MonadUnlockable l (MonadBB4GGInstance' c) (BB4GGInstanceT c m)
  where
  runUnlockedOrNot =
    runUnlockedOrNotWith @l $
      fmap (A.! reflectLockI @l) $
        BB4GGInstanceT . lift . view $
          _2 . locksI

instance MonadIO m => MonadObtain InstanceState (BB4GGInstanceT c m) where
  obtain =
    liftIO . readTVarIO =<< (BB4GGInstanceT . lift . view $ _2 . stateI)

instance
  MonadIO m
  => MonadState InstanceState (UnlockT LockISt (BB4GGInstanceT c m))
  where
  get =
    lift obtain
  put s =
    lift $
      liftIO . atomically . flip writeTVar s
        =<< (BB4GGInstanceT . lift . view $ _2 . stateI)
  state f =
    lift $
      liftIO . atomically . flip stateTVar f
        =<< (BB4GGInstanceT . lift . view $ _2 . stateI)

instance
  ( MonadBGG m
  , c (BB4GGInstanceT c m)
  , LiftsThroughEnough c
  )
  => MonadBB4GGInstance' c (BB4GGInstanceT c m)
  where
  instanceName =
    BB4GGInstanceT . lift $ reader fst
  saveInstanceState =
    do
      p <- lift . BB4GGInstanceT . lift . view $ _2 . dataPathI
      s <- obtain
      liftIO
        . BS.writeFile p
        . runPut
        . safePut
        . toSavableInstanceState
        $ s
  serverAppWS =
    BB4GGInstanceT . lift . view $ _2 . wsServerI . serverAppWSS
  broadcastWS msg =
    lift $
      liftIO . ($ msg)
        =<< (BB4GGInstanceT . lift . view $ _2 . wsServerI . broadcastWSS)
  hasClientsWS chan =
    liftIO . ($ chan)
      =<< (BB4GGInstanceT . lift . view $ _2 . wsServerI . hasClientsWSS)
  liftForkableMBI f =
    hoist (join . embedBGGT . hoist liftIO) . BB4GGInstanceT . readerBIT $
      f @MonadIO @(BB4GGInstanceT MonadIO (BGGT IO))

-- | This can't be derived because we're applying multiple monad transformators...
instance MFunctor (BB4GGInstanceT c) where
  hoist f (BB4GGInstanceT m) = BB4GGInstanceT $ hoist (hoist f) m

-- | This can't be derived because we're applying multiple monad transformators...
instance MonadTrans (BB4GGInstanceT c) where
  lift = BB4GGInstanceT . lift . lift

-- | Initializes an instance.
initInstance
  :: MonadIO m
  => FilePath
  -- ^ Path to the data directory.
  -> InstanceName
  -- ^ Name of the instance.
  -> InstanceState
  -- ^ Initial state.
  -> m Instance
initInstance dataPath name initialState =
  liftIO $
    do
      stateVar <- newTVarIO initialState
      -- Write the state.
      -- Even if the instance already existed this could change the file due to implicit version migration.
      BS.writeFile path
        . runPut
        . safePut
        . toSavableInstanceState
        $ initialState
      lockVars <-
        A.array (minBound, maxBound)
          <$> mapM (\l -> (l,) <$> newEmptyTMVarIO) enumerate
      wsServer <- initWebsocketsServer
      return
        Instance
          { _dataPathI = path
          , _stateI = stateVar
          , _locksI = lockVars
          , _wsServerI = wsServer
          }
  where
    path = instanceDataPath dataPath name
