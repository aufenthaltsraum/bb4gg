{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Core.Monad.Trans.BB4GG
  ( module BB4GG.Core.Monad.BB4GG
  , BB4GGT
  , initBB4GG
  , runBB4GGT
  ) where

import BB4GG.Config
import BB4GG.Core.Monad.BB4GG
import BB4GG.Types

import BB4GG.BGG.Request (BGGState, BGGT, runBGGT)
import BB4GG.Core.Constants (initialInstanceState, instanceNamesDataPath)
import BB4GG.Core.Monad.Trans.BB4GGInstance
  ( BB4GGInstanceT
  , LiftsThroughEnough
  , LockIMax
  , MonadBB4GGInstance
  , initInstance
  , runBB4GGInstanceT
  )
import BB4GG.Util.Monad.Trans.HeistState
  ( BB4GGHeistM
  , HeistStateT (..)
  , MonadHeistState
  , runHeistStateT
  )
import BB4GG.Util.Monad.Unlockable
  ( LiftsThroughUnlockT
  , MonadUnlockable (..)
  , runUnlockedOrNotWith
  )

import Control.Applicative (Alternative)
import Control.Concurrent.STM
  ( atomically
  , newEmptyTMVarIO
  , newTVarIO
  , readTVar
  , readTVarIO
  , writeTVar
  )
import Control.Lens (over, view, (^.))
import Control.Monad (MonadPlus, join)
import Control.Monad.Base (MonadBase (..))
import Control.Monad.Morph (MFunctor (..))
import Control.Monad.Reader (ReaderT (..), asks, runReaderT)
import Control.Monad.Trans (MonadIO, MonadTrans, lift, liftIO)
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Kind (Constraint, Type)
import Data.List (intercalate)
import Data.Map.Syntax ((##))
import Data.Time (getCurrentTime)
import Heist (HeistState)
import Heist.Interpreted (bindSplices, textSplice)
import Snap.Core (MonadSnap (..))

import Data.Map.Strict qualified as M
import Data.Set qualified as S
import Data.Text qualified as T

-- | Type synonym for the reader type of 'BB4GGT' (currently 'BB4GG') to make changing it easier.
type ReadBT = BB4GG

-- | A monad transformer that adds @'MonadBB4GG' c@ functionality to any monad that instantiates 'MonadIO'.
newtype BB4GGT (c :: (Type -> Type) -> Constraint) m a = BB4GGT
  { readerBT :: BGGT (HeistStateT (ReaderT ReadBT m)) a
  }
  deriving
    ( Alternative
    , Applicative
    , Functor
    , Monad
    , MonadBase b
    , MonadBaseControl b
    , MonadHeistState
    , MonadIO
    , MonadPlus
    , MonadSnap
    )

deriving instance (MonadIO m, MonadBaseControl IO m) => MonadBGG (BB4GGT c m)

-- | Run a 'BB4GGT'.
runBB4GGT
  :: forall c m a
   . BB4GGT c m a
  -> BGGState
  -> HeistState BB4GGHeistM
  -> ReadBT
  -> m a
runBB4GGT bt bggs = runReaderT . runHeistStateT (runBGGT (readerBT bt) bggs)

-- | Uses the lock '_stateLockBB4GG' from the global application data 'BB4GG'.
instance
  ( MonadIO m
  , MonadBaseControl IO m
  , c (BB4GGT c m)
  , c (BB4GGInstanceT c (BB4GGT c m))
  , LiftsThroughEnough c
  )
  => MonadUnlockable 0 (MonadBB4GG' c) (BB4GGT c m)
  where
  runUnlockedOrNot =
    runUnlockedOrNotWith @0 $ BB4GGT . lift . lift $ view stateLockBB4GG

-- Write the application state (i.e. the names of existing instances) to disk.
-- Should only be used with acquired lock to avoid race conditions where older data is written later.
saveState :: MonadBB4GG' c m => UnlockT' m ()
saveState =
  do
    path <- instanceNamesDataPath . (^. dirsBC . dataDC) <$> lift getConfig
    liftIO
      . writeFile path
      . intercalate "\n"
      . S.toList
      =<< lift getInstanceNames

instance
  ( MonadIO m
  , MonadBaseControl IO m
  , c (BB4GGT c m)
  , LiftsThroughUnlockT c
  , MonadIO (UnlockT' (BB4GGT c m))
  , MonadBB4GGInstance LockIMax c (BB4GGInstanceT c (BB4GGT c m))
  )
  => MonadBB4GG' c (BB4GGT c m)
  where
  getConfig = BB4GGT . lift . lift $ view configBB4GG
  getInstanceNames =
    fmap (M.keysSet . view instancesS)
      . liftIO
      . readTVarIO
      =<< (BB4GGT . lift . lift $ view stateBB4GG)
  newInstance name =
    do
      stateVar <- lift . BB4GGT . lift . lift $ view stateBB4GG
      s <- liftIO $ readTVarIO stateVar
      if name `M.notMember` (s ^. instancesS)
        then do
          config <- getConfig
          let dataPath =
                config ^. dirsBC . dataDC
              instanceConfiguration =
                toInstanceConfiguration $ config ^. instanceBC
          now <- liftIO getCurrentTime
          newInst <-
            liftIO . initInstance dataPath name . initialInstanceState now $
              instanceConfiguration
          liftIO . atomically . writeTVar stateVar $
            over instancesS (M.insert name newInst) s
          saveState
          return True
        else return False
  deleteInstance name =
    do
      stateVar <- lift . BB4GGT . lift . lift $ view stateBB4GG
      join . liftIO . atomically $
        do
          s <- readTVar stateVar
          if name `M.member` (s ^. instancesS)
            then do
              writeTVar stateVar $ over instancesS (M.delete name) s
              return $
                do
                  saveState
                  return True
            else return $ return False
  runInstance instM name =
    do
      insts <-
        view instancesS
          <$> (liftIO . readTVarIO =<< (BB4GGT . lift . lift $ view stateBB4GG))
      case insts M.!? name of
        Just inst ->
          do
            instanceHeistState <-
              BB4GGT . lift . HeistStateT . asks $
                bindSplices
                  ("instance-name" ## textSplice $ T.pack name)
            Just
              <$> runBB4GGInstanceT instM instanceHeistState (name, inst)
        Nothing ->
          return Nothing

-- | This can't be derived because we're applying multiple monad transformators...
instance MFunctor (BB4GGT c) where
  hoist f (BB4GGT m) = BB4GGT $ hoist (hoist $ hoist f) m

-- | This can't be derived because we're applying multiple monad transformators...
instance MonadTrans (BB4GGT c) where
  lift = BB4GGT . lift . lift . lift

-- | Initializes the global application state 'BB4GG'.
initBB4GG :: MonadIO m => Bb4ggConfig -> Instances -> m BB4GG
initBB4GG conf is =
  liftIO $
    do
      stateVar <- newTVarIO $ State {_instancesS = is}
      lockVar <- newEmptyTMVarIO
      return $
        BB4GG
          { _configBB4GG = conf
          , _stateBB4GG = stateVar
          , _stateLockBB4GG = lockVar
          }
