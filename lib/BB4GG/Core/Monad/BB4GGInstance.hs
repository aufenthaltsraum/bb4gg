{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}

module BB4GG.Core.Monad.BB4GGInstance
  ( module BB4GG.BGG.Request
  , IsLockI (..)
  , LiftsThroughEnough
  , LockIBg
  , LockIBr
  , LockIMax
  , LockISt
  , LockISv
  , LocksI
  , MonadBB4GGInstance' (..)
  , MonadBB4GGInstance
  , MonadObtain (..)
  , MonadState (get, put)
  , MonadUnlockable (..)
  , UnlockT
  , runUnlocked
  , saveInstanceStateUnlocked
  ) where

import BB4GG.Types

import BB4GG.BGG.Request
  ( MonadBGG (..)
  , requestBGGSearch
  , retrieveBGGThing
  , retrieveBGGThings
  )
import BB4GG.Util.Monad.HeistState (MonadHeistState)
import BB4GG.Util.Monad.Obtain (MonadObtain (..))
import BB4GG.Util.Monad.Unlockable
  ( LiftsThrough
  , LiftsThrough2
  , LiftsThroughUnlockT
  , MonadUnlockable (..)
  , MonadUnlockableLE
  , UnlockT
  , runUnlocked
  )

import Control.Concurrent.Forkable (ForkableMonad)
import Control.Monad.Except (ExceptT)
import Control.Monad.Morph (MFunctor (..))
import Control.Monad.Reader (ReaderT)
import Control.Monad.State (MonadState (..))
import Control.Monad.Trans (MonadIO, MonadTrans (..))

import Network.WebSockets qualified as WS

-- | The lock level one should use to do automatic background changes to the instance state.
type LockIBg = 3

-- | The lock level required to change the instance state.
type LockISt = 2

-- | The lock level required to save the instance state to disk.
type LockISv = 1

-- | The lock level required to broadcast over websockets.
type LockIBr = 0

-- | The maximum lock level
type LockIMax = LockIBg

-- | Reflect a (type level) lock level down to the value level.
class IsLockI l where
  reflectLockI :: LocksI

instance IsLockI LockIBr where reflectLockI = LockIBr
instance IsLockI LockISv where reflectLockI = LockISv
instance IsLockI LockISt where reflectLockI = LockISt
instance IsLockI LockIBg where reflectLockI = LockIBg

-- | A constraint for the constraints @c@ that can be used in @'MonadBB4GGInstance'' c@.
-- This guarantees that they are compatible with all the monad trasnformers that are applied to monads instantiating @'MonadBB4GGInstance'' c@.
class
  ( LiftsThroughUnlockT c
  , LiftsThrough2 ExceptT Monoid c
  , forall r. LiftsThrough (ReaderT r) c
  ) =>
  LiftsThroughEnough c

-- | @'LiftsThroughEnough' c@ is simply an intersection of other constraints.
-- It is not possibly to realize this as a type synonym due to some constraints of the type system.
instance
  ( LiftsThroughUnlockT c
  , LiftsThrough2 ExceptT Monoid c
  , forall r. LiftsThrough (ReaderT r) c
  )
  => LiftsThroughEnough c

-- | A class that encapsulates the necessary functions to interact with an instance.
-- There is a lock level for each possible value of 'LocksI'.
-- Interaction with the 'InstanceState' is either via a 'MonadObtain' interface for read access, or via a 'MonadState' interface for both read and write access.
-- The latter can only be used after unlocking lock level 'LockISt' though.
-- To have access to unlocking functionality @'MonadBB4GGInstance' l c@ for @l@ some lock level needs to be used instead.
class
  ( MonadHeistState m
  , MonadBGG m
  , MonadObtain InstanceState m
  , MonadState InstanceState (UnlockT LockISt m)
  , c m
  , LiftsThroughEnough c
  ) =>
  MonadBB4GGInstance' c m
    | m -> c
  where
  -- | The name of the instance.
  instanceName :: m InstanceName

  -- | Save the state to disk.
  -- Needs lock level 'LockISv' to be unlocked.
  saveInstanceState :: UnlockT LockISv m ()

  -- | This is for running the websockets server; only needs to be used once per instance.
  serverAppWS :: m WS.ServerApp

  -- | Broadcasts a message over the websockets channel @ch@.
  -- Needs lock level 'LockIBr' to be unlocked.
  broadcastWS :: HasProtocol ch => MessageType ch -> UnlockT LockIBr m ()

  -- Whether any clients are subscribed to the given websockets channel.
  hasClientsWS :: Channel -> m Bool

  -- | Embed an action of a (generic) monad which implements 'ForkableMonad' in addition to this class.
  -- This is necessary since 'Snap', which is the base of our monad stack, is not forkable.
  -- (This could be achieved with an associated type family instead of the forall quantification, but then we can't use overlappable instances anymore.)
  liftForkableMBI
    :: (forall c' f. (MonadBB4GGInstance LockIMax c' f, ForkableMonad f) => f a)
    -> m a

-- getComputationStatus :: m ComputationStatus
-- resetComputation     :: Bool -> UnlockT m ()

-- | @'MonadBB4GGInstance'' c@ and unlocking functionality up to lock level @l@.
type MonadBB4GGInstance l c m =
  (MonadBB4GGInstance' c m, MonadUnlockableLE l (MonadBB4GGInstance' c) m)

instance
  {-# OVERLAPPABLE #-}
  ( MonadBB4GGInstance' c m
  , MonadTrans t
  , MonadIO (t m)
  , c (t m)
  )
  => MonadBB4GGInstance' c (t m)
  where
  instanceName = lift instanceName
  saveInstanceState = hoist lift saveInstanceState
  serverAppWS = lift serverAppWS
  broadcastWS = hoist lift . broadcastWS
  hasClientsWS = lift . hasClientsWS
  liftForkableMBI f = lift $ liftForkableMBI f

-- getComputationStatus = lift getComputationStatus
-- resetComputation = hoist lift . resetComputation

-- Utility function; saves the instance state and takes care of the lock.
saveInstanceStateUnlocked :: MonadBB4GGInstance LockISv c m => m ()
saveInstanceStateUnlocked = runUnlocked @LockISv saveInstanceState
