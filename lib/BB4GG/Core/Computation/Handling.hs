module BB4GG.Core.Computation.Handling
  ( resetComputation
  , startComputation
  ) where

import BB4GG.Core
import BB4GG.Core.Computation

import BB4GG.Core.Computation.MILP (computeBestAssignments)
import BB4GG.Types.Util (hasActivePreference, readComputation, waitingPlayers)
import BB4GG.Util (toRoundMicroseconds)
import BB4GG.Util.Monad.Obtain (obtaining)
import BB4GG.Web.Websockets.Broadcast (broadcastResults)

import Control.Concurrent (killThread, threadDelay)
import Control.Concurrent.Forkable (forkIO)
import Control.Concurrent.MVar (newEmptyMVar)
import Control.Lens (allOf, assign, preuse, use, view, (^.), (^?), _1)
import Control.Monad (void)
import Control.Monad.Extra (whenM)
import Control.Monad.Trans (liftIO)
import Data.Maybe (isJust)
import Data.Time (addUTCTime, getCurrentTime)

import Data.Map.Strict qualified as M
import Data.Set qualified as S

-- | Compute new assignments or throw an appropriate error.
startComputation
  :: forall c m
   . (MonadBB4GGInstance LockIBr c m, c (UnlockT LockISt m))
  => UnlockT LockISt m ()
startComputation =
  -- We change the computation status as fast as possible so that subsequent calls to 'resultsPage' don't see a 'Recompute' (unless the computation is reset in the meantime).
  do
    mVar <- liftIO newEmptyMVar
    assign computationStatusIS (NotLockedCS $ Right mVar)
    state <- get
    let players = waitingPlayers state
        playersWithoutPreference =
          M.keysSet
            . M.filter
              ( not
                  . hasActivePreference (state ^. gamesIS)
                  . view (valueTS . prefPD)
              )
            $ players
        requireReady =
          state ^. configurationIS . requireReadyIC
        playersNotYetReady =
          M.keysSet
            . M.filter ((== WantsToPlayPS) . (^. valueTS . statusPD))
            $ players
    if
      | M.null players ->
          throwCompError NoActivePlayersCE
      | not $ S.null playersWithoutPreference ->
          throwCompError $ PlayersWithoutPreferenceCE playersWithoutPreference
      | isJust requireReady && not (S.null playersNotYetReady) ->
          throwCompError $ PlayersNotYetReadyCE playersNotYetReady
      | otherwise ->
          -- We should lock before starting the computation.
          -- Otherwise, if there is a request between the start of the computation and the locking of the results, the results might be shown as if they were not locked.
          do
            case requireReady of
              Just td | td > 0 ->
                do
                  now <- liftIO getCurrentTime
                  let unlockAt = td `addUTCTime` now
                  assign
                    computationStatusIS
                    (UnlockAtCS unlockAt mVar)
                  liftForkableMBI $
                    void $
                      forkIO $
                        do
                          liftIO . threadDelay . toRoundMicroseconds $ td
                          () <-
                            runUnlocked @LockISt $
                              whenM
                                ( (== Just unlockAt)
                                    <$> preuse (computationStatusIS . unlockTimeCS)
                                )
                                releaseLock
                          return ()
              _ -> return ()
            liftForkableMBI $
              forkBB4GGComputationT
                (computeBestAssignments state)
                mVar
                broadcastResults
                (runUnlocked @LockISt $ whenM noResultsM releaseLock)
  where
    throwCompError :: ComputationError -> UnlockT LockISt m ()
    throwCompError err =
      do
        assign computationStatusIS (NotLockedCS $ Left err)
        broadcastResults
    noResultsM :: MonadBB4GGInstance' d n => n Bool
    noResultsM =
      maybe
        False
        ( allOf
            (computedAssignmentsC . traverse . _1 . traverse . _1)
            (== SpecialGID NotPlayingSG)
        )
        <$> (readComputation =<< obtaining computationStatusIS)

-- | Kills the currently running computation thread if there is one and sets the computation status to 'Recompute'.
-- Also starts a new computation if there are clients subscribed to the websockets server.
resetComputation :: (MonadBB4GGInstance LockIBr c m, c (UnlockT LockISt m)) => UnlockT LockISt m ()
resetComputation =
  do
    mComp <- readComputation =<< use computationStatusIS
    liftIO . mapM_ killThread $ mComp ^? traverse . threadIdC . traverse
    assign computationStatusIS RecomputeCS
    broadcastResults
    whenM (hasClientsWS ResultsChannel) startComputation

-- | "Releases" the locking of results.
releaseLock
  :: (MonadBB4GGInstance LockIBr c m, c (UnlockT LockISt m))
  => UnlockT LockISt m ()
releaseLock =
  do
    computationStatus <- use computationStatusIS
    case computationStatus of
      UnlockAtCS _ mVar ->
        do
          assign computationStatusIS . NotLockedCS $ Right mVar
          broadcastResults
      RecomputeAtCS _ _ ->
        resetComputation
      _ ->
        return ()
