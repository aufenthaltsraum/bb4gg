module BB4GG.Core.AutoDeactivate (autoDeactivate) where

import BB4GG.Core

import BB4GG.Core.Computation.Handling (resetComputation)
import BB4GG.Util (batch)
import BB4GG.Util.Monad.Obtain (obtaining)
import BB4GG.Util.Monad.Unlockable (runUnlockedVoid)
import BB4GG.Web.Websockets.Broadcast (broadcastMain)

import Control.Lens (assign, filtered, modifying, set, use, (^.))
import Control.Monad (forM_, when)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Time (addUTCTime, getCurrentTime)

import Data.Map qualified as M

-- | Calls autoDeactivateInstance for each instance.
autoDeactivate :: MonadBB4GG MonadIO m => m ()
autoDeactivate =
  mapM_ (runInstance $ runUnlocked @LockISt autoDeactivateInstance)
    =<< getInstanceNames

-- | Sets those players to inactive whose last action is longer ago than the time specified in the config.
-- Similarly removes an assignment if it hasn't been changed for some time.
autoDeactivateInstance :: MonadBB4GGInstance LockISt c m => m ()
autoDeactivateInstance =
  do
    now <- liftIO getCurrentTime
    mAutoDeactivationHours <-
      obtaining $ configurationIS . autoDeactivationHoursIC
    forM_ mAutoDeactivationHours $ \autoDeactivationHours ->
      runUnlockedVoid @LockISt $
        do
          batch $
            do
              let autoDeactivationNDT =
                    fromIntegral (autoDeactivationHours * 3600)
                  pastAutoDeactivation t =
                    autoDeactivationNDT `addUTCTime` t < now
              modifying
                ( playersIS
                    . traverse
                    . filtered
                      ( \pd ->
                          (pd ^. valueTS . statusPD) /= InactivePS
                            && pastAutoDeactivation (pd ^. timestampTS)
                      )
                )
                (set (valueStampedTS now . statusPD) InactivePS)
              assig <- use currentAssigIS
              when
                ( not (M.null $ assig ^. valueTS)
                    && pastAutoDeactivation (assig ^. timestampTS)
                )
                (assign (currentAssigIS . valueStampedTS now) M.empty)
          resetComputation
          broadcastMain
