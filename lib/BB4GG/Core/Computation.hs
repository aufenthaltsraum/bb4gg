module BB4GG.Core.Computation
  ( BB4GGComputationT
  , MonadBB4GGComputation (..)
  , forkBB4GGComputationT
  ) where

import BB4GG.Types

import BB4GG.Types.Util (hashAssignment)

import Control.Concurrent.Forkable (ForkableMonad, forkIO)
import Control.Concurrent.MVar (MVar, modifyMVar_, tryPutMVar)
import Control.Lens (over, set)
import Control.Monad (when)
import Control.Monad.Base (MonadBase (..))
import Control.Monad.Reader (ReaderT (..), runReaderT)
import Control.Monad.Trans (MonadIO, liftIO)
import Data.Time.Clock.POSIX (getPOSIXTime)

import Data.Map.Strict qualified as M

-- | A class that encapsulates all necessary functions to interact with a computation.
class MonadIO m => MonadBB4GGComputation m where
  addComputedAssignment :: (Assignment, AssignmentScore) -> m ()

-- | A monad transformer that adds 'MonadBB4GGComputation' functionality to any monad that instantiates 'MonadIO'.
newtype BB4GGComputationT m a = BB4GGComputationT
  { readerBCT :: ReaderT (MVar Computation, m ()) m a
  }
  deriving
    ( Applicative
    , Functor
    , Monad
    , MonadBase b
    , MonadIO
    )

-- | Run a 'BB4GGComputationT'.
-- Only for internal use.
runBB4GGComputationT
  :: BB4GGComputationT m a
  -- ^ The computation to be executed
  -> MVar Computation
  -- ^ The MVar to be updated.
  -> m ()
  -- ^ An action to be executed on computation of new results.
  -> m a
runBB4GGComputationT c = curry . runReaderT $ readerBCT c

-- | Fork a thread that executes a 'BB4GGComputationT'.
-- This also correctly initializes the MVar beforehand.
forkBB4GGComputationT
  :: (ForkableMonad m, MonadIO m)
  => BB4GGComputationT m ()
  -- ^ The computation to be executed
  -> MVar Computation
  -- ^ The MVar to be updated.
  -> m ()
  -- ^ An action to be executed on computation of new computation results.
  -> m ()
  -- ^ An action to be executed when the computations ends.
  -> m ()
forkBB4GGComputationT c mVar newResultsAction endAction =
  do
    mVarIsPut <-
      liftIO . tryPutMVar mVar $
        Computation
          { _threadIdC = Nothing
          , _computedAssignmentsC = M.empty
          }
    when mVarIsPut $
      do
        newResultsAction
        threadId <-
          forkIO $
            do
              runBB4GGComputationT c mVar newResultsAction
              liftIO . modifyMVar_ mVar $ return . set threadIdC Nothing
              endAction
              newResultsAction
        liftIO . modifyMVar_ mVar $
          return . set threadIdC (Just threadId)

instance MonadIO m => MonadBB4GGComputation (BB4GGComputationT m) where
  addComputedAssignment result@(assig, _) =
    BB4GGComputationT . ReaderT $ \(compMVar, newResultsAction) ->
      do
        time <- liftIO getPOSIXTime
        liftIO
          . modifyMVar_ compMVar
          . (return .)
          . over computedAssignmentsC
          $ M.insert (hashAssignment time assig) result
        newResultsAction
