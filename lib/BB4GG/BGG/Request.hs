{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}

module BB4GG.BGG.Request
  ( BGGState
  , BGGT
  , MonadBGG (embedBGGT, retrieveBGGThings)
  , initBGG
  , requestBGGCollection
  , requestBGGSearch
  , retrieveBGGThing
  , retrieveBGGThingsInChunks
  , runBGGT
  ) where

import BB4GG.BGG.Types
import BB4GG.Config
import BB4GG.Util.Timestamped

import BB4GG.BGG.Parse (parseBGGCollection, parseBGGSearch, parseBGGThings)
import BB4GG.Util (foldMapA, toRoundMicroseconds)
import BB4GG.Util.Cache
  ( Cache
  , entriesC
  , insertMultiple
  , isStale
  , mkCache
  , valueCE
  )
import BB4GG.Util.Monad.HeistState (MonadHeistState)

import Control.Applicative (Alternative)
import Control.Arrow ((&&&))
import Control.Concurrent (MVar, newMVar, putMVar, takeMVar, threadDelay)
import Control.Concurrent.Forkable (ForkableMonad)
import Control.Concurrent.STM
  ( TVar
  , atomically
  , modifyTVar'
  , newTVarIO
  , readTVar
  , readTVarIO
  , writeTVar
  )
import Control.Exception (catch, throw)
import Control.Exception.Lifted (bracket_, finally)
import Control.Lens (makeLenses, view, (^.))
import Control.Monad (MonadPlus, join, (<=<))
import Control.Monad.Base (MonadBase)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Morph (MFunctor)
import Control.Monad.Reader (ReaderT, asks, runReaderT)
import Control.Monad.Trans (MonadTrans, lift)
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Bifunctor (bimap)
import Data.Either (rights)
import Data.Either.Extra (eitherToMaybe, mapLeft, mapRight)
import Data.Functor.Identity (runIdentity)
import Data.List (intercalate)
import Data.List.Extra (chunksOf)
import Data.Maybe (fromMaybe)
import Data.Time (NominalDiffTime, UTCTime, addUTCTime, diffUTCTime, getCurrentTime)
import Network.HTTP.Client
  ( HttpException (..)
  , HttpExceptionContent (ResponseTimeout, StatusCodeException)
  , Response
  , responseBody
  , responseStatus
  )
import Network.HTTP.Types (statusCode)
import Network.Wreq.Session (Session, get, newAPISession)
import Snap.Core (MonadSnap)
import System.Timeout.Lifted (timeout)

import Data.ByteString.Lazy qualified as BS
import Data.Map.Strict qualified as M
import Data.Set qualified as S

-- | Provides the ability to fetch items from the BGG API.
class MonadIO m => MonadBGG m where
  -- | Sends a request to the BGG API.
  -- This should handle a 429 response (Too Many Requests), and return 'Nothing' on any other error.
  requestBGG :: String -> m (Either HttpExceptionContent BS.ByteString)

  -- | Tries to fetch the items belonging to the given ids.
  retrieveBGGThings
    :: S.Set BGGID -> m (M.Map BGGID (Either BGGException BGGThing))

  -- | Needed for lifting from 'ForkableMonad'.
  embedBGGT :: BGGT n a -> m (n a)

instance
  {-# OVERLAPPABLE #-}
  ( MonadBGG m
  , MonadTrans t
  , MonadIO (t m)
  )
  => MonadBGG (t m)
  where
  retrieveBGGThings = lift . retrieveBGGThings
  requestBGG = lift . requestBGG
  embedBGGT = lift . embedBGGT

-- | Combines 'requestBGG' and 'parseBGGSearch'.
-- URL special characters in the search query are ignored.
requestBGGSearch
  :: MonadBGG m
  => Bool
  -- ^ Whether the search should only yield exact hits.
  -> S.Set BGGThingType
  -- ^ The thing types to search for.
  -> String
  -- ^ The search query.
  -> m (Either BGGException [BGGSearchResult])
requestBGGSearch _ types _ | S.null types = return $ Right []
requestBGGSearch exact types query =
  fmap parse . requestBGG $
    "search"
      ++ "?type="
      ++ (intercalate "," . map showBGGThingType $ S.toList types)
      ++ (if exact then "&exact=1" else "")
      ++ "&query="
      ++ query'
  where
    query' = filter (not . (`elem` ("$&+,/:;=?@#%" :: String))) query
    parse = join . bimap BGGHttpException (bimap BGGXMLException rights . parseBGGSearch)

-- | Fetches the items marked as "Owned" in the collection of a given user.
requestBGGCollection
  :: MonadBGG m
  => BGGThingType
  -- ^ The thing type to look for.
  -> String
  -- ^ The username.
  -> m (Either BGGException [BGGID])
requestBGGCollection thingType user =
  fmap parse . requestBGG $
    "collection"
      ++ "?username="
      ++ user
      ++ "&subtype="
      ++ showBGGThingType thingType
      ++ "&brief=1&own=1"
      -- Necessary due to a bug in the API
      ++ if thingType == BoardgameBGGTT then "&excludesubtype=boardgameexpansion" else ""
  where
    parse = join . bimap BGGHttpException (bimap BGGXMLException rights . parseBGGCollection)

-- | This is 'retrieveBGGThings' for a single game.
retrieveBGGThing
  :: MonadBGG m => BGGID -> m (Either BGGException (Maybe BGGThing))
retrieveBGGThing i = sequence . M.lookup i <$> retrieveBGGThings (S.singleton i)

-- | A monad transformer that adds BGG caching and rate control functionality.
newtype BGGT m a = BGGT
  { readerBGGT :: ReaderT BGGState m a
  }
  deriving
    ( Alternative
    , Applicative
    , ForkableMonad
    , Functor
    , MFunctor
    , Monad
    , MonadBase b
    , MonadBaseControl b
    , MonadHeistState
    , MonadIO
    , MonadPlus
    , MonadSnap
    , MonadTrans
    )

-- | Execute a 'BGGT'.
runBGGT :: BGGT m a -> BGGState -> m a
runBGGT = runReaderT . readerBGGT

-- | The state of the interaction with BGG.
data BGGState = BGGState
  { _cacheBGGT :: TVar (Cache BGGID BGGThing)
  -- ^ A cache for items from BGG.
  , _delayBGGT :: TVar (Timestamped NominalDiffTime)
  -- ^ The delay between requests requests currently used.
  , _waitUntilBGGT :: TVar UTCTime
  -- ^ Time at which enough time since the last request will have passed to make a new one.
  , _requestHandleBGGT :: MVar ()
  -- ^ This MVar being empty indicates that a request to BGG is ongoing.
  , _sessionBGGT :: Session
  -- ^ Session used for BGG requests.
  , _configBGGT :: BggConfig
  -- ^ The config.
  }

makeLenses ''BGGState

-- | Initializes the 'BGGState' using the given configuration.
initBGG :: MonadIO m => BggConfig -> m BGGState
initBGG conf =
  do
    cacheVar <-
      liftIO . newTVarIO $
        mkCache
          Nothing -- We assume that the BGG cache has no limit
          (fromIntegral $ (conf ^. cacheDaysBC) * 24 * 60 * 60)
    delayVar <- liftIO $ newTVarIO =<< stampIO (conf ^. minDelayBC)
    waitUntilVar <- liftIO $ newTVarIO =<< getCurrentTime
    handleVar <- liftIO . newMVar $ ()
    session <- liftIO newAPISession
    return $
      BGGState
        { _cacheBGGT = cacheVar
        , _delayBGGT = delayVar
        , _waitUntilBGGT = waitUntilVar
        , _requestHandleBGGT = handleVar
        , _sessionBGGT = session
        , _configBGGT = conf
        }

-- Sends a GET request to the BGG API.
-- Only for internal use.
getBGG
  :: MonadIO m => String -> BGGT m (Either HttpExceptionContent (Response BS.ByteString))
getBGG query =
  do
    session <- BGGT $ view sessionBGGT
    liftIO $ (Right <$> get session url) `catch` handler
  where
    url = "https://boardgamegeek.com/xmlapi2/" ++ query
    handler (HttpExceptionRequest _ hec) = return $ Left hec
    handler e@(InvalidUrlException _ _) = throw e

-- | Combines 'requestBGG' and 'parseBGGThings'.
-- This does not provide any caching; only for internal use!
-- Use 'retrieveBGGThings' instead.
requestBGGThingChunk
  :: (MonadIO m, MonadBaseControl IO m)
  => [BGGID]
  -> BGGT m (M.Map BGGID (Either BGGException BGGThing))
requestBGGThingChunk idChunk =
  either
    (allFailed . BGGHttpException)
    ( either
        (allFailed . BGGXMLException)
        (M.map $ mapLeft BGGParseException)
        . parseBGGThings
    )
    <$> requestBGG
      ("thing?stats=1&id=" ++ intercalate "," (show <$> idChunk))
  where
    allFailed e = M.fromList $ map (,Left e) idChunk

-- | Splits the given set in chunks of given size and retrieves each chunk with a separate call to 'retrieveBGGThings'.
--  In particular, the timeout of 'retrieveBGGThings' applies to each chunk and not the whole operation.
retrieveBGGThingsInChunks
  :: MonadBGG m => S.Set BGGID -> m (M.Map BGGID (Either BGGException BGGThing))
retrieveBGGThingsInChunks ids =
  do
    chunkSize <- fmap runIdentity . embedBGGT . BGGT . view $ configBGGT . maxThingsToRequestBC
    foldMapA (retrieveBGGThings . S.fromList) . chunksOf chunkSize . S.toList $ ids

-- | Computes the current delay of requests from the current time and the previous delay.
currentDelay
  :: BggConfig -> UTCTime -> Timestamped NominalDiffTime -> NominalDiffTime
currentDelay conf now delayStamped =
  max (conf ^. minDelayBC) $ (delayStamped ^. valueTS) * realToFrac factor
  where
    timeDiff = now `diffUTCTime` (delayStamped ^. timestampTS)
    factor :: Double
    factor = (conf ^. delayDecreasePerMinBC) ** realToFrac (timeDiff / 60)

instance (MonadIO m, MonadBaseControl IO m) => MonadBGG (BGGT m) where
  -- This implementation retrieves BGG things from the cache, fetching ones that are stale or not already present via requestBGG.
  -- The error cases in fetching are handled as follows.
  -- If the cache entry for an ID doesn't exist and fetching it yields an error, the error is added to the cache and returned.
  -- If the cache entry for an ID is stale and fetching it yields an error, the old value is kept in the cache and returned.
  -- If an ID that is to be fetched is absent in the fetch result, the associated cache entry is updated to not exist and the ID will be missing in the returned map.
  -- If the retrieval timeout is reached before all IDs to be fetched could be processed, the remaining ones are not updated in the cache.
  -- For such IDs, the stale value is returned if present and a ResponseTimeout error otherwise.
  retrieveBGGThings ids =
    do
      now <- liftIO getCurrentTime
      cacheVar <- BGGT $ view cacheBGGT
      cache <- liftIO $ readTVarIO cacheVar
      chunkSize <- BGGT . view $ configBGGT . maxThingsToRequestBC
      let initialEntries = M.fromSet (view entriesC cache M.!?) ids
          chunks =
            chunksOf chunkSize . M.keys . M.filter (maybe True $ isStale cache now) $ initialEntries
          initialResults = M.mapMaybe (fmap Right . view valueCE =<<) initialEntries
          processTimeout results remainingChunks =
            do
              let timedOutResults =
                    M.fromList . map (,Left . BGGHttpException $ ResponseTimeout) . concat $
                      remainingChunks
              return $ M.unionWith eitherAlt timedOutResults results
          processChunks _ results [] = return results
          processChunks timeoutNDT results allChunks@(chunk : moreChunks)
            | timeoutNDT <= 0 = processTimeout results allChunks
            | otherwise =
                do
                  before <- liftIO getCurrentTime
                  mNewResults <-
                    timeout (toRoundMicroseconds timeoutNDT) . requestBGGThingChunk $ chunk
                  case mNewResults of
                    Nothing -> processTimeout results allChunks
                    Just newResults ->
                      do
                        let updatedResults = M.unionWith eitherAlt newResults results
                            entriesToUpdate =
                              M.fromList . map (id &&& (eitherToMaybe <=< (newResults M.!?))) $
                                chunk
                        liftIO . atomically . modifyTVar' cacheVar . insertMultiple now $
                          entriesToUpdate
                        after <- liftIO getCurrentTime
                        let newTimeout = timeoutNDT - (after `diffUTCTime` before)
                        processChunks newTimeout updatedResults moreChunks
      timeoutNDT <- BGGT . view $ configBGGT . retrievalTimeoutBC
      processChunks timeoutNDT initialResults chunks
    where
      eitherAlt (Right a) _ = Right a
      eitherAlt _ (Right b) = Right b
      eitherAlt (Left e) _ = Left e
  requestBGG query =
    do
      delayVar <- BGGT $ view delayBGGT
      waitUntilVar <- BGGT $ view waitUntilBGGT
      requestHandleVar <- BGGT $ view requestHandleBGGT
      conf <- BGGT $ view configBGGT
      let updateWaitUntil =
            liftIO $ do
              now <- getCurrentTime
              atomically $
                writeTVar waitUntilVar . (`addUTCTime` now) . view valueTS =<< readTVar delayVar
          go =
            do
              waitUntil <- liftIO . readTVarIO $ waitUntilVar
              beforeRequest <- liftIO getCurrentTime
              liftIO . threadDelay . toRoundMicroseconds $ waitUntil `diffUTCTime` beforeRequest
              eResponse <- getBGG query
              afterRequest <- liftIO getCurrentTime
              -- The response codes 429 (Too Many Requests) and 202 (Accepted, i.e. processing) both mean we should try again after a delay.
              if getStatusCode eResponse `elem` [Just 429, Just 202]
                then do
                  liftIO . atomically $ do
                    delay <- view valueTS <$> readTVar delayVar
                    let newDelay = realToFrac (conf ^. delayIncreaseOn429BC) * delay
                    writeTVar delayVar . stamp afterRequest $ newDelay
                  updateWaitUntil
                  go
                else do
                  liftIO . atomically $ do
                    delay <- readTVar delayVar
                    let newDelay = currentDelay conf afterRequest delay
                    writeTVar delayVar . stamp afterRequest $ newDelay
                  return $ mapRight responseBody eResponse
          getStatusCode (Left (StatusCodeException r _)) = Just $ statusCode (responseStatus r)
          getStatusCode (Right r) = Just $ statusCode (responseStatus r)
          getStatusCode _ = Nothing
      bracket_
        (liftIO . takeMVar $ requestHandleVar)
        (updateWaitUntil `finally` liftIO (putMVar requestHandleVar ()))
        $ fromMaybe (Left ResponseTimeout)
          <$> timeout (toRoundMicroseconds $ view retrievalTimeoutBC conf) go
  embedBGGT bt = BGGT . asks $ runBGGT bt
