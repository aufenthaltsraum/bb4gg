{-# LANGUAGE RecordWildCards #-}

module BB4GG.BGG.Extract (extractGame, importBGGIDs, updateGamesWithBGG) where

import BB4GG.Core.Constants
import BB4GG.Types

import BB4GG.BGG.Request
  ( MonadBGG
  , requestBGGSearch
  , retrieveBGGThingsInChunks
  )
import BB4GG.Util (insertNew)

import Control.Lens (over, set, view, (^.))
import Control.Monad ((<=<))
import Data.Either.Extra (eitherToMaybe, rights)
import Data.Maybe (isNothing)
import Data.Monoid (Any (Any))
import Data.Ord (clamp)
import Data.Tuple.Extra (both)

import Data.Map.Strict qualified as M
import Data.Set qualified as S
import Data.Text qualified as T

-- | Returns a function that adds the BGG id of each game if it is missing and searching for its name yields only a single exact hit (and the game wasn't modified in the meantime).
updateGamesWithBGG
  :: MonadBGG m
  => Bool
  -- ^ Whether to also set the recommended player numbers.
  -> S.Set BGGThingType
  -- ^ The types to search for.
  -> Games
  -> m (Games -> Games)
updateGamesWithBGG setRecom types games
  | M.null toSearch || S.null types = return id
  | otherwise =
      do
        searchResults <- traverse (doSearch . view nameGD) toSearch
        let uniqueHits =
              M.mapMaybe
                (fmap (view idBGGSR) . single <=< eitherToMaybe)
                searchResults
        bggGames <-
          fmap (M.mapMaybe eitherToMaybe)
            . retrieveBGGThingsInChunks
            . S.fromList
            $ M.elems uniqueHits
        let bggGames' = M.mapMaybe (bggGames M.!?) uniqueHits
        return $ \games' ->
          M.foldrWithKey
            ( \gID bggGame gs ->
                M.adjustWithKey
                  (updateWithBGGGame bggGame)
                  gID
                  gs
            )
            games'
            bggGames'
  where
    toSearch = M.filter (isNothing . view bggIDGD) games
    single [a] = Just a
    single _ = Nothing
    doSearch = requestBGGSearch True types
    updateWithBGGGame bggGame gID gd'
      | gd' == games M.! gID =
          (if setRecom then transcribeRecom bggGame else id) $
            set bggIDGD (Just $ bggGame ^. idBGGT) gd'
    updateWithBGGGame _ _ gd = gd

-- | Returns a function that adds the specified BGG games (if the game already exists, it adds an additional copy).
importBGGIDs :: MonadBGG m => [BGGID] -> m (Games -> Games)
importBGGIDs ids =
  do
    things <- rights . M.elems <$> retrieveBGGThingsInChunks (S.fromList ids)
    return $ \gs -> foldr add gs things
  where
    addCopies thing =
      traverse
        ( \gd ->
            if gd ^. bggIDGD == Just (thing ^. idBGGT)
              then (Any True, over maxCopiesGD (+ 1) gd)
              else (Any False, gd)
        )
    add thing gds =
      case addCopies thing gds of
        (Any True, gds') -> gds'
        (Any False, _) -> insertNew (extractGame thing) gds

-- | Extract a `GameData` from a `BGGThing`.
extractGame :: BGGThing -> GameData
extractGame BGGThing {..} =
  GameData
    { _nameGD = take maxGameNameLength $ T.unpack _nameBGGT
    , _minPlayersGD = minP
    , _maxPlayersGD = maxP
    , _recommendedMinPlayersGD =
        max minP . maybe minP (clamp (1, userLimit)) $ S.lookupMin _recommendedPlayersBGGT
    , _recommendedMaxPlayersGD =
        min maxP . maybe maxP (clamp (1, userLimit)) $ S.lookupMax _recommendedPlayersBGGT
    , _maxCopiesGD = 1
    , _isAvailableGD = True
    , _commentGD = ""
    , _bggIDGD = Just _idBGGT
    }
  where
    (minP, maxP) = both (clamp (1, userLimit)) $
      case (_minPlayersBGGT, _maxPlayersBGGT) of
        (Just mi, Just ma) | mi <= ma -> (mi, ma)
        (Just mi, Nothing) -> (mi, mi)
        (Nothing, Just ma) -> (ma, ma)
        _ -> (1, 1)

-- | Transcribes the recommended player numbers from the BGG game.
transcribeRecom :: BGGThing -> GameData -> GameData
transcribeRecom bggGame gd =
  maybe
    id
    (set recommendedMinPlayersGD . clampPlayers)
    (S.lookupMin $ bggGame ^. recommendedPlayersBGGT)
    $ maybe
      id
      (set recommendedMaxPlayersGD . clampPlayers)
      (S.lookupMax $ bggGame ^. recommendedPlayersBGGT)
      gd
  where
    clampPlayers = clamp (gd ^. minPlayersGD, gd ^. maxPlayersGD)
