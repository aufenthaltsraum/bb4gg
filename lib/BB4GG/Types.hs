{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types
  ( module BB4GG.Types
  , module BB4GG.BGG.Types
  , module BB4GG.Types.Game
  , module BB4GG.Types.Player
  , module BB4GG.Types.Assignment
  , module BB4GG.Types.Computation
  , module BB4GG.Types.Instance
  ) where

import BB4GG.BGG.Types
import BB4GG.Types.Assignment
import BB4GG.Types.Computation
import BB4GG.Types.Game
import BB4GG.Types.Instance
import BB4GG.Types.Player

import BB4GG.Config (Bb4ggConfig)

import Control.Concurrent (ThreadId)
import Control.Concurrent.STM (TMVar, TVar)
import Control.Lens (makeLenses)

-- | The state of the application.
newtype State = State
  { _instancesS :: Instances
  -- ^ The current instances.
  }

makeLenses ''State

-- | Global data for running the application.
data BB4GG = BB4GG
  { _configBB4GG :: Bb4ggConfig
  , _stateBB4GG :: TVar State
  , _stateLockBB4GG :: TMVar ThreadId
  }

makeLenses ''BB4GG
