{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types.Assignment where

import BB4GG.Types.Game
import BB4GG.Types.Player

import Control.Lens (makeLenses)
import Data.Aeson.TH (defaultOptions, deriveJSON, fieldLabelModifier)

import Data.Map.Strict qualified as M
import Data.Set qualified as S

-- | The identifier of an instance of a game in an assignment.
-- Will be a hashed value.
type AssignmentGameID = Int

-- | Contains the data of an assignment of players to games.
type Assignment = M.Map AssignmentGameID (GameID, S.Set PlayerID)

-- | The identifier of an assignment.
-- Will be a hashed value.
type AssignmentID = Int

-- | The score of each player for a given assignment.
newtype AssignmentScore = AssignmentScore
  { _playerScoresAS :: M.Map PlayerID Score
  }

makeLenses ''AssignmentScore
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''AssignmentScore)

-- | A collection of assignments with their score, indexed by id.
type Assignments = M.Map AssignmentID (Assignment, AssignmentScore)
