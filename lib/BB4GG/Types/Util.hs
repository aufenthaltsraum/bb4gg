module BB4GG.Types.Util
  ( assignedPlayers
  , assignmentScore
  , copiesInAssignment
  , displayPlayerStatus
  , gamePreferenceScore
  , gamePreferenceScores
  , gameScoreInfo
  , hasActivePreference
  , hashAssignment
  , hashAssignmentGame
  , isGamePreferred
  , isInGame
  , isPlayerInactive
  , minimumAS
  , ordAS
  , preferredGameIDs
  , preferringPlayers
  , readComputationStatus
  , readComputation
  , readMaybePlayerStatus
  , requiresReady
  , showPlayerStatus
  , sumAS
  , waitingPlayers
  ) where

import BB4GG.Core.Constants
import BB4GG.Types

import BB4GG.Util (fromSetMaybe)

import Control.Applicative ((<|>))
import Control.Arrow (second, (&&&))
import Control.Concurrent.MVar (readMVar)
import Control.Lens
  ( At (at)
  , asIndex
  , filtered
  , folded
  , itraversed
  , ix
  , maximumOf
  , preview
  , to
  , view
  , (^.)
  , (^?)
  , _2
  )
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Hashable (hashWithSalt)
import Data.Ix (inRange)
import Data.Maybe (fromMaybe, isJust)
import Data.Set.Lens (setOf)
import Data.String (IsString)
import Data.Time.Clock.POSIX (POSIXTime)

import Data.Map.Strict qualified as M
import Data.Set qualified as S

-- | Whether `requireReadyIC` in the instance configuration is a `Just`.
requiresReady :: InstanceState -> Bool
requiresReady = isJust . view (configurationIS . requireReadyIC)

-- | A human readable string representation of a 'PlayerStatus''.
displayPlayerStatus :: IsString s => PlayerStatus -> s
displayPlayerStatus ReadyPS = "Ready"
displayPlayerStatus WantsToPlayPS = "Want to play"
displayPlayerStatus InactivePS = "Inactive"

-- | A machine readable string representation of a 'PlayerStatus''.
showPlayerStatus :: IsString s => PlayerStatus -> s
showPlayerStatus ReadyPS = "ready"
showPlayerStatus WantsToPlayPS = "wantsToPlay"
showPlayerStatus InactivePS = "inactive"

-- | Reading the machine readable string representation of a 'PlayerStatus'.
readMaybePlayerStatus :: (Eq s, IsString s) => s -> Maybe PlayerStatus
readMaybePlayerStatus "ready" = Just ReadyPS
readMaybePlayerStatus "wantsToPlay" = Just WantsToPlayPS
readMaybePlayerStatus "inactive" = Just InactivePS
readMaybePlayerStatus _ = Nothing

-- | The score of a game preference with a given player number (or 0 if not prefered).
gamePreferenceScore :: Int -> GamePreference -> Score
gamePreferenceScore playerNum pref
  | pref ^. isActiveGP =
      fromMaybe 0 $
        M.lookup playerNum (pref ^. extraScoresGP)
          <|> ( if inRange
                  (pref ^. minPlayersGP, pref ^. maxPlayersGP)
                  playerNum
                  then Just $ pref ^. scoreGP
                  else Nothing
              )
  | otherwise = 0

-- | The assignment of player number to score for a given game preference (only positive scores are included).
gamePreferenceScores :: GamePreference -> M.Map Int Score
gamePreferenceScores pref =
  M.filter (> 0) $
    M.union
      (pref ^. extraScoresGP)
      ( M.fromDistinctAscList $
          map (,pref ^. scoreGP) [pref ^. minPlayersGP .. pref ^. maxPlayersGP]
      )

-- | Get the preference scores of the given players for the given assignment.
assignmentPreferences :: Players -> Assignment -> M.Map PlayerID Score
assignmentPreferences players = foldMap gameScores
  where
    gameScores (gID, pIDs) =
      fromSetMaybe
        ( \pID ->
            players
              ^? ix pID
                . valueTS
                . prefPD
                . at gID
                . to (maybe 0 . gamePreferenceScore $ length pIDs)
        )
        pIDs

-- | Get the score of an assignment.
assignmentScore :: Players -> Assignment -> AssignmentScore
assignmentScore players assignment =
  AssignmentScore
    { _playerScoresAS = assignmentPreferences players assignment
    }

-- | Get the minimum of an assignment score.
minimumAS :: AssignmentScore -> Score
minimumAS = minimum . (maxPreference :) . M.elems . _playerScoresAS

-- | Get the sum of an assignment score.
sumAS :: AssignmentScore -> Score
sumAS = sum . _playerScoresAS

-- | Compare assignments scores, first by sum, then by minimum.
-- This is not an 'Ord' istance since it is not a total order.
ordAS :: AssignmentScore -> (Score, Score)
ordAS = sumAS &&& minimumAS

isGamePreferred :: GameID -> PlayerData -> Bool
isGamePreferred gID PlayerData {_prefPD = pref} =
  maybe False (view isActiveGP) $ pref M.!? gID

-- | Extract those players who have a preference for the game with the given ID from given players.
preferringPlayers :: Players -> GameID -> Players
preferringPlayers ps gID = M.filter (isGamePreferred gID . view valueTS) ps

-- | Those games for which at least one of the players has an active preference.
preferredGameIDs :: Players -> S.Set GameID
preferredGameIDs =
  setOf $
    traverse
      . valueTS
      . prefPD
      . itraversed
      . filtered (view isActiveGP)
      . asIndex

-- | The players which are both not inactive and not assigned to play a game.
waitingPlayers :: InstanceState -> Players
waitingPlayers state =
  M.filter (not . isPlayerInactive . view valueTS)
    . (view playersIS state M.\\)
    $ assignedPlayers state

-- | Checks whether a player is marked as inactive.
isPlayerInactive :: PlayerData -> Bool
isPlayerInactive = (== InactivePS) . view statusPD

-- | Whether a 'Preference' contains an active preference for an available game.
hasActivePreference :: Games -> Preference -> Bool
hasActivePreference gs =
  any (view isActiveGP) . M.filterWithKey (\gID _ -> isAvailable gID)
  where
    isAvailable gID = Just True == gs ^? gameDataG gID . isAvailableGD

-- | The players which are currently assigned to play a game.
assignedPlayers :: InstanceState -> Players
assignedPlayers state =
  M.restrictKeys
    (state ^. playersIS)
    (setOf (currentAssigIS . valueTS . traverse . _2 . folded) state)

-- | Whether a player is currently playing a game.
isInGame :: InstanceState -> PlayerID -> Bool
isInGame state pID = M.member pID $ assignedPlayers state

-- | Count the number of copies of a given game in an assignment.
copiesInAssignment :: GameID -> Assignment -> Int
copiesInAssignment gID = length . M.filter ((== gID) . fst)

-- | Computes a hash of an assignment with a given salt.
hashAssignment :: POSIXTime -> Assignment -> AssignmentID
hashAssignment salt =
  hashWithSalt (round $ salt * 1000) . M.toList . fmap (second S.toList)

-- | Computes a hash of an instance of a game in an assignment with a given salt.
hashAssignmentGame :: POSIXTime -> (GameID, S.Set PlayerID) -> AssignmentGameID
hashAssignmentGame salt = hashWithSalt (round $ salt * 1000) . second S.toList

-- | Calls 'readMVar' on the 'MVar' inside the 'ComputationStatus' if there is one.
-- If there is an MVar, but it is empty, this blocks the thread until it is full!
readComputation :: MonadIO m => ComputationStatus -> m (Maybe Computation)
readComputation = fmap (preview compCS) . readComputationStatus

-- | Calls 'readMVar' on the 'MVar' inside the 'ComputationStatus' if there is one.
-- If there is an MVar, but it is empty, this blocks the thread until it is full!
readComputationStatus
  :: MonadIO m => ComputationStatus -> m (ComputationStatus' Computation)
readComputationStatus = liftIO . mapM readMVar

-- | Returns, for a given game, the number of active preferring players, their total score, and the average score among them.
gameScoreInfo :: InstanceState -> GameID -> (Players, Score, Double)
gameScoreInfo state gID = (players, scoreSum, averageScore)
  where
    players = preferringPlayers (waitingPlayers state) gID
    scoreSum = sum $ maxScore <$> players
    maxScore =
      fromMaybe 0
        . maximumOf
          (valueTS . prefPD . ix gID . to gamePreferenceScores . traverse)
    averageScore = fromIntegral scoreSum / fromIntegral (max 1 $ length players)
