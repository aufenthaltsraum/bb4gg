{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types.Player
  ( module BB4GG.Types.Player
  , module BB4GG.Util.Timestamped
  ) where

import BB4GG.Types.Game
import BB4GG.Util.Timestamped

import Control.Lens (makeLenses, makePrisms, (^.))
import Data.Aeson.TH (defaultOptions, deriveJSON, fieldLabelModifier)
import Data.Function (on)
import Data.SafeCopy (base, deriveSafeCopy)

import Data.Map.Strict qualified as M

-- | Score for player preferences and assignments.
type Score = Int

-- | Contains preference data for a single game.
data GamePreference = GamePreference
  { _scoreGP :: Score
  -- ^ Preference score (0 = not at all).
  , _minPlayersGP :: Int
  -- ^ Preferred number of minimum players.
  , _maxPlayersGP :: Int
  -- ^ Preferred number of maximum players.
  , _extraScoresGP :: M.Map Int Score
  -- ^ Scores for special player numbers
  , _isActiveGP :: Bool
  -- ^ Whether the preference is active.
  }
  deriving (Eq, Show)

makeLenses ''GamePreference

deriveSafeCopy 1 'base ''GamePreference
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''GamePreference)

-- | Representation of preferences for multiple games.
type Preference = M.Map GameID GamePreference

-- | Representation of the status of a player.
-- The time is the last point when the player was active.
data PlayerStatus = ReadyPS | WantsToPlayPS | InactivePS
  deriving (Eq, Ord, Show)

makePrisms ''PlayerStatus

deriveSafeCopy 1 'base ''PlayerStatus
$(deriveJSON defaultOptions ''PlayerStatus)

-- | Contains the data associated to a player.
data PlayerData = PlayerData
  { _namePD :: String
  -- ^ Name of the player.
  , _statusPD :: PlayerStatus
  -- ^ Whether the player is ready to play, wants to play or inactive.
  , _prefPD :: Preference
  -- ^ Preferences of the player.
  }
  deriving (Eq, Show)

makeLenses ''PlayerData

deriveSafeCopy 1 'base ''PlayerData
$(deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''PlayerData)

-- | Compares players first by whether they are marked as \"in game\", then by their names.
instance Ord PlayerData where
  compare = compare `on` info
    where
      info pd = (pd ^. statusPD, pd ^. namePD)

-- | Identifier of a player.
type PlayerID = Word

-- | Representation of multiple players.
type Players = M.Map PlayerID (Timestamped PlayerData)
