module BB4GG.Util
  ( batch
  , insertNew
  , foldMapA
  , periodicAction
  , sortMap
  , toRoundMicroseconds
  , toRoundSeconds
  , rangeToString
  , fromSetMaybe
  ) where

import Control.Concurrent.AlarmClock (newAlarmClock', setAlarm, setAlarmNow)
import Control.Monad.State (MonadState, State, runState, state)
import Data.List (sortOn)
import Data.Time (NominalDiffTime, addUTCTime)
import Data.Tuple (swap)

import Data.Map.Strict qualified as M
import Data.Set qualified as S

-- | Add a new element to a map.
insertNew :: (Ord k, Num k) => a -> M.Map k a -> M.Map k a
insertNew x m = M.insert (maybe 0 fst (M.lookupMax m) + 1) x m

-- | Create a list whose members are the results of applying the given function to the key-value pairs of the given map and which is ordered by the original values in the map.
sortMap :: (Ord k, Ord a) => ((k, a) -> b) -> M.Map k a -> [b]
sortMap f = map f . sortOn swap . M.toList

-- | Round 'NominalDiffTime' to number of microseconds.
toRoundMicroseconds :: NominalDiffTime -> Int
toRoundMicroseconds = round . (* 1000000)

-- | Round 'NominalDiffTime' to number of seconds.
toRoundSeconds :: NominalDiffTime -> Integer
toRoundSeconds = round

-- | Executes an action periodically with given time interval.
periodicAction
  :: NominalDiffTime
  -- ^ Time interval.
  -> IO ()
  -- ^ Action to execute periodically.
  -> IO ()
periodicAction interval action =
  setAlarmNow
    =<< newAlarmClock'
      (\ac t -> action >> setAlarm ac (interval `addUTCTime` t))

-- | A utility function to batch multiple state actions into a single call of 'state'.
-- This is useful if each call to 'state' is atomic.
batch :: MonadState s m => State s a -> m a
batch = state . runState

-- | Prints @\"a-b\"@ or just @\"a\"@, depending on whether @a@ and @b@ are different or not.
rangeToString :: (Eq a, Show a) => (a, a) -> String
rangeToString (a, b)
  | a == b = show a
  | otherwise = show a ++ "-" ++ show b

-- | Apply a function to elements of a set and collect the 'Just' values in a 'Map'.
fromSetMaybe :: (k -> Maybe a) -> S.Set k -> M.Map k a
fromSetMaybe f s = M.mapMaybe f $ M.fromSet id s

-- | Traverse the foldable while concatenating the results.
foldMapA :: (Applicative m, Foldable f, Monoid b) => (a -> m b) -> f a -> m b
foldMapA f = foldr (liftA2 (<>) . f) $ pure mempty
